
import { Evaluator } from "../../entities/evaluator";
import { EvaluatorType } from "../../entities/evaluator-type";
import { Filiere } from "../../entities/filiere";
import { Project } from "../../entities/project";
import { Student } from "../../entities/student";

export function generateAttributionDataset1(): [Project[], Evaluator[], Student[]] {
	const projects: Project[] = [
		new Project("p1", "project1", 1, Filiere.none),
		new Project("p2", "project2", 2, Filiere.none),
		new Project("p3", "project3", 3, Filiere.asi),
		new Project("p4", "project4", 4, Filiere.asi),
		new Project("p5", "project5", 5, Filiere.il),
	];

	const evaluators: Evaluator[] = [
		new Evaluator("e1", "", "", EvaluatorType.other),
		new Evaluator("e2", "", "", EvaluatorType.other),
		new Evaluator("e3", "", "", EvaluatorType.other),
		new Evaluator("e4", "", "", EvaluatorType.other),
		new Evaluator("e5", "", "", EvaluatorType.other),
		new Evaluator("e6", "", "", EvaluatorType.other),
	];

	const students: Student[] = [
		new Student("s1", "", "", "", 1, Filiere.none),
	];

	evaluators[0].setChild(students[0]);
	projects[0].addStudent(students[0]);

	evaluators[0].viewingWishes.push(Filiere.asi);
	evaluators[0].viewingWishes.push(Filiere.il);
	evaluators[1].viewingWishes.push(Filiere.asi);
	evaluators[2].viewingWishes.push(Filiere.il);
	evaluators[4].viewingWishes.push(Filiere.il);
	evaluators[4].viewingWishes.push(Filiere.asi);

	return [projects, evaluators, students];
}
