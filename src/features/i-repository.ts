import { Criteria } from "../entities/criteria";
import { Evaluator } from "../entities/evaluator";
import { Grade } from "../entities/grade";
import { Project } from "../entities/project";
import { Student } from "../entities/student";

export interface IRepository<T> {
	entities: T[];

	nextId(): string;
	create(entity: T): void;
	update(id: string, values: T): void;

	getById(id: string): T | null;
}

export interface ICriteriaRepository extends IRepository<Criteria> {
	getByName(name: string): Criteria | null;
}

export interface IEvaluatorRepository extends IRepository<Evaluator> {
	getByEmail(email: string): Evaluator | null;
	getByToken(token: string): Evaluator | null;
	authenticate(connexionToken: string, tokenExpiry: number): void;
	createAttribution(evaluator: Evaluator, project: Project, manual: boolean): void;
	deleteAttribution(evaluator: Evaluator, project: Project): void;
}

export interface IGradeRepository extends IRepository<Grade> { }

export interface IProjectRepository extends IRepository<Project> {
	getActiveProjects(): Project[];
	activateProject(project: Project): void;
	deactivateProject(project: Project): void;
}

export interface IStudentRepository extends IRepository<Student> {
	getByFirstLastName(firstname: string, lastname: string): Student | null;
}
