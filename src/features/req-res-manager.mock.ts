import { Evaluator } from "../entities/evaluator";
import { IRequestManager, IResponseManager } from "./i-req-res-manager";
import { Pathname } from "./pathname";
import { WebPage } from "./web-page";

export class RequestManager implements IRequestManager {
	public evaluator?: Evaluator;

	setEvaluator(evaluator: Evaluator): void {
		this.evaluator = evaluator;
	}
}

export class ResponseManager implements IResponseManager {
	public setCookieName?: string;
	public setCookieValue?: string;
	public setCookieMaxAge?: number;
	public clearedTokenName?: string;
	public nextMiddleware?: boolean;
	public redirectPath?: Pathname;
	public renderWebPage?: WebPage;

	public setCookie(name: string, value: string, maxAge: number): void {
		this.setCookieName = name;
		this.setCookieValue = value;
		this.setCookieMaxAge = maxAge;
	}

	public clearCookie(name: string): void {
		this.clearedTokenName = name;
	}

	public next(): void {
		this.nextMiddleware = true;
	}

	public redirect(path: Pathname): void {
		this.redirectPath = path;
	}

	public render(page: WebPage): void {
		this.renderWebPage = page;
	}
}
