// All the page the server has.

export enum WebPage {
	undecided = "undecided",

	// Virtual pages determined depending on the state of the application
	home = "home",

	// Concrete pages
	login = "login",
	mustWatch = "mustWatch",
	allVideos = "allVideos",
	emailNotFound = "emailNotFound"
}

export enum ResponseType {
	display = "display",
	redirect = "redirect",
	next = "next",
}
