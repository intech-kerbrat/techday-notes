
export interface ITokenProvider {
	getToken(): string;
}

export interface ICSVProvider {
	parse(value: string): any;
	parse(value: string, separator: string): any;
}

// TODO: DateProvider?
