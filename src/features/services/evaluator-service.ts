// ===== Imports ===== //

import { Evaluator } from "../../entities/evaluator";
import { EvaluatorType } from "../../entities/evaluator-type";
import { Filiere } from "../../entities/filiere";
import { Grade } from "../../entities/grade";
import { Project } from "../../entities/project";
import { Student } from "../../entities/student";
import { ICSVProvider, ITokenProvider } from "../i-provider";
import { ICriteriaRepository, IEvaluatorRepository, IGradeRepository, IProjectRepository, IStudentRepository } from "../i-repository";
import { IResponseManager } from "../i-req-res-manager";
import { Pathname } from "../pathname";
import { ResponseManager } from "../req-res-manager.mock";
import { WebPage } from "../web-page";
import { AttributionService } from "./attribution-service";

// ===== Service ===== //

export interface IAttributionForm {
	evaluator_id: string;
	project_id: string;
}

export interface IGenerateAttributionForm {
	evaluatorsGoal: number;
	projectsGoal: number;
}

export interface IEvaluateForm {
	evaluator_id: string;
	project_id: string;
	grades: number[];
	remarks: string;
}

export interface IEditGradesForm {
	evaluator_id: string;
	grades_id: string[];
	grades: number[];
	remarks: string;
}

export class EvaluatorService {
	constructor(
		private csvProvider: ICSVProvider,
		private tokenProvider: ITokenProvider,
		private criteriaRepository: ICriteriaRepository,
		private evaluatorRepository: IEvaluatorRepository,
		private gradeRepository: IGradeRepository,
		private projectRepository: IProjectRepository,
		private studentRepository: IStudentRepository,
	) { }

	/**
	 * Import four kind of jury: parent, student, employee and alumni.
	 * @param juryCSV The export of jurys from the survey.
	 */
	public importJurys(response: IResponseManager, juryCSV: string): void {
		try {
			const jurys: string[][] = this.csvProvider.parse(juryCSV);
			jurys.splice(0, 1);

			for (let row of jurys) {
				const email: string = row[1];

				if (this.evaluatorRepository.getByEmail(email) !== null) {
					continue;
				}

				const id: string = this.evaluatorRepository.nextId();
				const name: string = row[2];
				const type: EvaluatorType = EvaluatorService.parseEvaluatorType(row[3]);
				const wishes: string[] = row[4].split(";");
				const child_name: string = row[5];
				const evaluator = new Evaluator(id, email, name, type);

				if (child_name !== "") {
					const [firstname, lastname] = Student.parseFirstAndLastNames(child_name);
					const child = this.studentRepository.getByFirstLastName(firstname, lastname);

					if (child !== null) {
						evaluator.setChild(child);
					}
				}

				if (wishes.some(wish => wish.includes("Architecture"))) {
					evaluator.viewingWishes.push(Filiere.asi);
				}

				if (wishes.some(wish => wish.includes("Logicielle"))) {
					evaluator.viewingWishes.push(Filiere.il);
				}

				this.evaluatorRepository.create(evaluator);
			}
		} catch (error) {
			console.error(error);
			throw error;
		} finally {
			response.redirect(Pathname.adminImports);
		}
	}

	/**
	 * Import partners types of evaluators.
	 * @param partnersCSV The export of partners from the survey.
	 */
	public importPartners(response: ResponseManager, partnersCSV: string) {
		try {
			const partners: string[][] = this.csvProvider.parse(partnersCSV, ";");
			partners.splice(0, 1);

			for (let row of partners) {
				const email: string = row[3];

				if (this.evaluatorRepository.getByEmail(email) !== null) {
					continue;
				}

				const id: string = this.evaluatorRepository.nextId();
				const name: string = row[1];
				const type: EvaluatorType = EvaluatorType.partner;
				const evaluator = new Evaluator(id, email, name, type);

				if (row[5]?.includes("Architecture")) {
					evaluator.viewingWishes.push(Filiere.asi);
				}

				if (row[6]?.includes("Ingénierie")) {
					evaluator.viewingWishes.push(Filiere.il);
				}

				this.evaluatorRepository.create(evaluator);
			}
		} catch (error) {
			console.error(error);
			throw error;
		} finally {
			response.redirect(Pathname.adminImports);
		}
	}

	/**
	 * Authenticate the evaluator.
	 * @param email The submitted email by the user.
	 */
	public login(response: IResponseManager, email: string): void {
		const token = this.tokenProvider.getToken();
		const evaluator = this.evaluatorRepository.getByEmail(email);

		if (evaluator === null) {
			response.render(WebPage.emailNotFound);
			return;
		}

		const maxAge = evaluator.authenticate(token);
		this.evaluatorRepository.authenticate(token, evaluator.tokenExpiry as number);

		response.setCookie("connexion_token", token, maxAge);
		response.redirect(Pathname.home);
	}

	/**
	 * Determines the page to display for an arriving user to the web site.
	 * @param evaluator The connected user trying to access to the home page.
	 */
	public displayHome(response: IResponseManager, evaluator: Evaluator | undefined): void {
		if (evaluator === undefined || evaluator.isConnected() === false) {
			response.redirect(Pathname.login);
		} else {
			response.render(WebPage.mustWatch);
		}
	}

	/**
	 * Add a project to the attributions of an evaluator.
	 * @param form The form's values to create the attribution.
	 */
	public createAttribution(response: IResponseManager, form: IAttributionForm): void {
		const evaluator = this.evaluatorRepository.getById(form.evaluator_id);
		const project = this.projectRepository.getById(form.project_id);

		if (evaluator && project && project.active) {
			this.evaluatorRepository.createAttribution(evaluator, project, true);
		}

		response.redirect(Pathname.adminEvaluators);
	}

	/**
	 * Delete a project to the attributions of an evaluator.
	 * @param form The form's values to delete the attribution.
	 */
	public deleteAttribution(response: IResponseManager, form: IAttributionForm): void {
		const evaluator = this.evaluatorRepository.getById(form.evaluator_id);
		const project = this.projectRepository.getById(form.project_id);

		if (evaluator && project) {
			this.evaluatorRepository.deleteAttribution(evaluator, project);
		}

		response.redirect(Pathname.adminEvaluators);
	}

	/**
	 * Automaticaly create attributions for every projects.
	 */
	public generateAttributions(response: IResponseManager, form: IGenerateAttributionForm): void {
		const evaluators = this.evaluatorRepository.entities;
		const projects = this.projectRepository.getActiveProjects();
		const nodes: [Evaluator, Filiere][] = [];

		for (let evaluator of evaluators) {
			for (let i = 0; i < form.evaluatorsGoal; i++) {
				if (evaluator.viewingWishes[i] !== undefined) {
					nodes.push([evaluator, evaluator.viewingWishes[i]]);
				} else if (evaluator.viewingWishes.length === 1 && i === 1) {
					nodes.push([evaluator, evaluator.viewingWishes[0]]);
				} else {
					nodes.push([evaluator, Filiere.any]);
				}
			}
		}

		const attributionService = new AttributionService(evaluators, projects);

		for (let node of nodes) {
			const project = attributionService.findNextCompatibleProject(node);
			this.evaluatorRepository.createAttribution(node[0], project, false);
		}

		while (attributionService.getLowestProjectAttributionCount(projects) < form.projectsGoal) {
			const project = attributionService.getLowestProjectAttribution(projects);
			const evaluator = attributionService.findNextCompatibleEvaluator(project);
			this.evaluatorRepository.createAttribution(evaluator, project, false);
		}

		response.redirect(Pathname.admin);
	}

	/**
	 * Register the grades.
	 * @param form The grades values from the form.
	 */
	public evaluateProject(response: ResponseManager, form: IEvaluateForm) {
		const evaluator = this.evaluatorRepository.getById(form.evaluator_id);
		const project = this.projectRepository.getById(form.project_id);
		const criterias = this.criteriaRepository.entities;

		if (!evaluator || !project) {
			console.error(`Could not get evaluator or project`);
			console.error(form);
			response.redirect(Pathname.home);
			return;
		}

		if (evaluator.hasGradesForProject(project, criterias) === false) {
			const criterias = this.criteriaRepository.entities;

			for (let i = 0; i < criterias.length; i++) {
				const grade = new Grade(this.criteriaRepository.nextId(), project, criterias[i], form.grades[i]);
				evaluator.setGrade(grade);
				this.gradeRepository.create(grade);
			}

			evaluator.remarks.push({
				remark: form.remarks,
				project: project,
			});

			this.evaluatorRepository.update(evaluator.id, evaluator);
		}

		response.redirect(Pathname.home);
	}

	public editGrades(response: IResponseManager, form: IEditGradesForm): void {
		const evaluator = this.evaluatorRepository.getById(form.evaluator_id);
		const criterias = this.criteriaRepository.entities;

		if (!evaluator) {
			console.error(`Could not get evaluator`);
			console.error(form);
			response.redirect(Pathname.home);
			return;
		}

		let project: Project | null = null;

		for (let i = 0; i < form.grades_id.length; i++) {
			const grade = evaluator.grades.find(grade => grade.id === form.grades_id[i]);

			if (grade) {
				project = grade.project;
				grade.value = form.grades[i];
				this.gradeRepository.update(form.grades_id[i], grade);
			}
		}

		if (project !== null) {
			const remark = evaluator.remarks.find(remarks => remarks.project === project);

			if (remark) {
				remark.remark = form.remarks;
				this.evaluatorRepository.update(form.evaluator_id, evaluator);
			}
		}

		response.redirect(evaluator.hasWatchedAllVideos(criterias) ? Pathname.projects : Pathname.home);
	}

	/**
	 * Parse strings of the survey's responses to extract the evaluator's type.
	 * @param type The survey's answer.
	 */
	public static parseEvaluatorType(type: string): EvaluatorType {
		if (type.startsWith("Parent")) {
			return EvaluatorType.parent;
		} else if (type.startsWith("Etudiant")) {
			return EvaluatorType.student;
		} else if (type.startsWith("Salarié")) {
			return EvaluatorType.employee;
		} else if (type.startsWith("Ancien")) {
			return EvaluatorType.alumni;
		}

		return EvaluatorType.other;
	}
}
