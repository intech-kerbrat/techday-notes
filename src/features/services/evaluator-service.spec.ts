
// ===== Imports ===== //

import { assert } from "chai";
import { readFileSync } from "fs";
import "mocha";
import parse from "csv-parse/lib/sync";

import { Criteria } from "../../entities/criteria";
import { Evaluator } from "../../entities/evaluator";
import { EvaluatorType } from "../../entities/evaluator-type";
import { Filiere } from "../../entities/filiere";
import { Grade } from "../../entities/grade";
import { Project, VideoType } from "../../entities/project";
import { Student } from "../../entities/student";
import { CriteriaRepository } from "../../stores/criteria-repository";
import { EvaluatorRepository } from "../../stores/evaluator-repository";
import { ProjectRepository } from "../../stores/project-repository";
import { StudentRepository } from "../../stores/student-repository";
import { generateAttributionDataset1 } from "../../test-data/attributions/attribution_dataset_1";
import { EvaluatorService, IAttributionForm, IEditGradesForm, IEvaluateForm } from "./evaluator-service";
import { ICSVProvider, ITokenProvider } from "../i-provider";
import { Pathname } from "../pathname";
import { ResponseManager } from "../req-res-manager.mock";
import { WebPage } from "../web-page";
import { GradeRepository } from "../../stores/grade-repository";
import { AttributionService } from "./attribution-service";

// ===== Mocks and filler data ===== //

class UUIDProvider implements ITokenProvider {
	getToken(): string {
		return "xxx";
	}
}

class CSVProvider implements ICSVProvider {
	parse(value: string, separator: string = ","): any {
		return parse(value, { delimiter: separator });
	}
}

const uuidProvider = new UUIDProvider();
const csvProvider = new CSVProvider();

const juryCSV = readFileSync("./src/test-data/jury.csv", "utf-8");
const partnersCSV = readFileSync("./src/test-data/partners.csv", "utf-8");

const criterias: Criteria[] = [
	new Criteria("c2", "c1", "text"),
	new Criteria("c2", "c2", "text"),
];

let evaluators: Evaluator[];

const projects: Project[] = [
	new Project("p1", "project1", 1, Filiere.none),
	new Project("p2", "project2", 1, Filiere.none),
	new Project("p3", "project3", 1, Filiere.none),
];

// ===== Tests ===== //

describe("EvaluatorService", () => {
	let criteriaRepository: CriteriaRepository;
	let evaluatorRepository: EvaluatorRepository;
	let gradeRepository: GradeRepository;
	let projectRepository: ProjectRepository;
	let studentRepository: StudentRepository;
	let response: ResponseManager;
	let sut: EvaluatorService;

	beforeEach(() => {
		evaluators = [
			new Evaluator("e1", "", "", EvaluatorType.other),
			new Evaluator("e2", "", "", EvaluatorType.other),
			new Evaluator("e3", "", "", EvaluatorType.other),
			new Evaluator("e4", "", "", EvaluatorType.other),
		];

		criteriaRepository = new CriteriaRepository();
		evaluatorRepository = new EvaluatorRepository();
		gradeRepository = new GradeRepository();
		projectRepository = new ProjectRepository();
		studentRepository = new StudentRepository();
		response = new ResponseManager();
		sut = new EvaluatorService(csvProvider, uuidProvider, criteriaRepository, evaluatorRepository, gradeRepository, projectRepository, studentRepository);
	});

	describe("importJurys", () => {
		let child: Student;

		beforeEach(() => {
			child = new Student("a", "Etudiant", "UN", "un@site.com", 1, Filiere.none);
			studentRepository.create(child);
			sut.importJurys(response, juryCSV);
		});

		it("should import parent type evaluators", () => {
			assert.isNotNull(evaluatorRepository.getByEmail("jury1@site.com"));
			assert.strictEqual(evaluatorRepository.getByEmail("jury1@site.com")?.type, EvaluatorType.parent);
		});

		it("should link up the parent to the student", () => {
			assert.strictEqual(evaluatorRepository.getByEmail("jury1@site.com")?.children, child);
		});

		it("should import student type evaluators", () => {
			assert.isNotNull(evaluatorRepository.getByEmail("jury2@site.com"));
			assert.strictEqual(evaluatorRepository.getByEmail("jury2@site.com")?.type, EvaluatorType.student);
		});

		it("should import emplyee type evaluators", () => {
			assert.isNotNull(evaluatorRepository.getByEmail("jury3@site.com"));
			assert.strictEqual(evaluatorRepository.getByEmail("jury3@site.com")?.type, EvaluatorType.employee);
		});

		it("should import alumni type evaluators", () => {
			assert.isNotNull(evaluatorRepository.getByEmail("jury4@site.com"));
			assert.strictEqual(evaluatorRepository.getByEmail("jury4@site.com")?.type, EvaluatorType.alumni);
		});

		it("should redirect to the 'admin' path", () => {
			assert.strictEqual(response.redirectPath, Pathname.adminImports);
		});

		it("should not duplicate evaluators entities for jury", () => {
			const juryCSV2 = [
				"H,A,PN,V,S",
				"18/01/2021 21:06:00,jury2@site.com,Jury DEUX,Etudiant INTECH,",
				"18/01/2021 21:06:00,jury2@site.com,Jury DEUX,Etudiant INTECH,"
			].join("\n");

			sut.importJurys(response, juryCSV2);

			const juryCount = evaluatorRepository.entities.reduce((acc, evaluator) => evaluator.email === "jury2@site.com" ? acc + 1 : acc, 0);
			assert.strictEqual(juryCount, 1);
		});

		it("should import ASI and IL viewing wished for jury 2", () => {
			assert.isNotNull(evaluatorRepository.getByEmail("jury2@site.com"));
			assert.isTrue(evaluatorRepository.getByEmail("jury2@site.com")?.viewingWishes.some(wish => wish === Filiere.asi));
			assert.isTrue(evaluatorRepository.getByEmail("jury2@site.com")?.viewingWishes.some(wish => wish === Filiere.il));
		});

		it("should import ASI viewing wished for jury 3", () => {
			assert.isNotNull(evaluatorRepository.getByEmail("jury3@site.com"));
			assert.isTrue(evaluatorRepository.getByEmail("jury3@site.com")?.viewingWishes.some(wish => wish === Filiere.asi));
		});

		it("should import IL viewing wished for jury 4", () => {
			assert.isNotNull(evaluatorRepository.getByEmail("jury4@site.com"));
			assert.isTrue(evaluatorRepository.getByEmail("jury4@site.com")?.viewingWishes.some(wish => wish === Filiere.il));
		});
	});

	describe("importPartners", () => {
		it("should import partner type evaluators", () => {
			sut.importPartners(response, partnersCSV);
			assert.isNotNull(evaluatorRepository.getByEmail("part1@nom.fr"));
			assert.strictEqual(evaluatorRepository.getByEmail("part1@nom.fr")?.type, EvaluatorType.partner);
		});

		it("should redirect to the 'admin' path", () => {
			sut.importPartners(response, partnersCSV);
			assert.strictEqual(response.redirectPath, Pathname.adminImports);
		});

		it("should not duplicate evaluators entites for partners", () => {
			const partnersCSV2 = [
				"E;NP;F;M;NT;J",
				"Entreprise;NOM Prénom;Développeur;prenom@nom.fr;0123456789;oui",
				"Entreprise;NOM Prénom;Développeur;prenom@nom.fr;0123456789;oui"
			].join("\n");

			sut.importPartners(response, partnersCSV2);

			const partnersCount = evaluatorRepository.entities.reduce((acc, evaluator) => evaluator.email === "prenom@nom.fr" ? acc + 1 : acc, 0);
			assert.strictEqual(partnersCount, 1);
		});

		it("should import ASI and IL viewing wished for jury 2", () => {
			sut.importPartners(response, partnersCSV);
			assert.isNotNull(evaluatorRepository.getByEmail("part2@nom.fr"));
			assert.isTrue(evaluatorRepository.getByEmail("part2@nom.fr")?.viewingWishes.some(wish => wish === Filiere.asi));
			assert.isTrue(evaluatorRepository.getByEmail("part2@nom.fr")?.viewingWishes.some(wish => wish === Filiere.il));
		});

		it("should import ASI viewing wished for jury 3", () => {
			sut.importPartners(response, partnersCSV);
			assert.isNotNull(evaluatorRepository.getByEmail("part3@nom.fr"));
			assert.isTrue(evaluatorRepository.getByEmail("part3@nom.fr")?.viewingWishes.some(wish => wish === Filiere.asi));
		});

		it("should import IL viewing wished for jury 4", () => {
			sut.importPartners(response, partnersCSV);
			assert.isNotNull(evaluatorRepository.getByEmail("part4@nom.fr"));
			assert.isTrue(evaluatorRepository.getByEmail("part4@nom.fr")?.viewingWishes.some(wish => wish === Filiere.il));
		});
	});

	describe("login", () => {
		it("should login an evaluator with its email and redirect to the 'home' page", () => {
			const evaluator = new Evaluator("a", "abc@site.com", "abc", EvaluatorType.parent);
			evaluatorRepository.create(evaluator);

			sut.login(response, "abc@site.com");

			assert.strictEqual(response.redirectPath, Pathname.home);
		});

		it("should return the connexion token and the cookie max age", () => {
			const evaluator = new Evaluator("a", "abc@site.com", "abc", EvaluatorType.parent);
			evaluatorRepository.create(evaluator);

			sut.login(response, "abc@site.com");

			assert.strictEqual(response.setCookieName, "connexion_token");
			assert.strictEqual(response.setCookieValue, "xxx");
			assert.strictEqual(response.setCookieMaxAge, evaluator.tokenMaxAge);
		});

		it("should display the 'email not found' page if the evaluator was not found in the repository", () => {
			sut.login(response, "abc@site.com");
			assert.strictEqual(response.renderWebPage, WebPage.emailNotFound);
		});
	});

	describe("displayHome", () => {
		it("should redirect to the 'login' page if the evaluator is not connected", () => {
			const evaluator = new Evaluator("a", "abc@site.com", "abc", EvaluatorType.parent);
			evaluatorRepository.create(evaluator);

			sut.displayHome(response, evaluator);

			assert.strictEqual(response.redirectPath, Pathname.login);
		});

		it("should display the 'must watch' page if the evaluator has not yet evaluated all its videos", () => {
			const evaluator = new Evaluator("a", "abc@site.com", "abc", EvaluatorType.parent);
			evaluator.registerProjectsToEvaluate([projects[0]], false);
			evaluator.grades.push(new Grade("a", projects[0], criterias[0], 1));
			evaluatorRepository.create(evaluator);
			criteriaRepository.create(criterias[0]);
			criteriaRepository.create(criterias[1]);
			sut.login(response, "abc@site.com");

			sut.displayHome(response, evaluator);

			assert.strictEqual(response.renderWebPage, WebPage.mustWatch);
		});

		it("should display the 'all videos' page if the evaluator has already evaluated all its videos", () => {
			const evaluator = new Evaluator("a", "abc@site.com", "abc", EvaluatorType.parent);
			evaluator.registerProjectsToEvaluate([projects[0]], false);
			evaluator.grades.push(new Grade("a", projects[0], criterias[0], 1));
			evaluator.grades.push(new Grade("a", projects[0], criterias[1], 1));
			evaluatorRepository.create(evaluator);
			criteriaRepository.create(criterias[0]);
			criteriaRepository.create(criterias[1]);
			sut.login(response, "abc@site.com");

			sut.displayHome(response, evaluator);

			assert.strictEqual(response.renderWebPage, WebPage.mustWatch);
		});
	});

	let form: IAttributionForm;

	describe("createAttribution", () => {
		beforeEach(() => {
			form = {
				evaluator_id: evaluators[0].id,
				project_id: projects[0].id,
			};
		});

		it("should add the project to the evaluator", () => {
			evaluatorRepository.create(evaluators[0]);
			projectRepository.create(projects[0]);

			sut.createAttribution(response, form);

			assert.isTrue(evaluators[0].hasToEvaluate(projects[0]));
		});

		it("should redirect to the 'adminEvaluators' path", () => {
			sut.deleteAttribution(response, form);
			assert.strictEqual(response.redirectPath, Pathname.adminEvaluators);
		});

		it("should refuse to create an attribution for a deactivated project", () => {
			evaluatorRepository.create(evaluators[0]);
			const project = new Project("p4", "n4", 4, Filiere.il, "", VideoType.hosted, false);
			projectRepository.create(project);

			sut.createAttribution(response, { evaluator_id: evaluators[0].id, project_id: project.id });

			assert.isFalse(evaluators[0].to_evaluate.some((attribution) => attribution.project === project));
		});
	});

	describe("deleteAttribution", () => {
		beforeEach(() => {
			form = {
				evaluator_id: evaluators[0].id,
				project_id: projects[0].id,
			};
		});

		it("should add the project to the evaluator", () => {
			// Arrange
			evaluatorRepository.create(evaluators[0]);
			projectRepository.create(projects[0]);
			sut.createAttribution(response, form);

			// Act
			sut.deleteAttribution(response, form);

			// Assert
			assert.isFalse(evaluators[0].hasToEvaluate(projects[0]));
		});

		it("should redirect to the 'adminEvaluators' path", () => {
			sut.deleteAttribution(response, form);
			assert.strictEqual(response.redirectPath, Pathname.adminEvaluators);
		});
	});

	describe("generateAttributions", () => {
		const evaluatorsGoal = 2;
		const projectsGoal = 3;
		let ds1_evaluators: Evaluator[];
		let ds1_projects: Project[];
		let ds1_students: Student[];

		beforeEach(() => {
			// NOTE tkerbrat: The child test might fail if there is less than four evaluators. It should never happen in practice.
			[ds1_projects, ds1_evaluators, ds1_students] = generateAttributionDataset1();
			ds1_projects.forEach(project => projectRepository.create(project));
			ds1_evaluators.forEach(evaluator => evaluatorRepository.create(evaluator));
		});

		it("should generate attribution to meet evaluators and projects goals", () => {
			sut.generateAttributions(response, { evaluatorsGoal, projectsGoal });

			const attributionService = new AttributionService(ds1_evaluators, ds1_projects);

			const evaluatorsMin = attributionService.getLowestEvaluatorAttributionCount(ds1_evaluators);
			const projectsMin = attributionService.getLowestProjectAttributionCount(ds1_projects);
			assert.isAtLeast(evaluatorsMin, evaluatorsGoal);
			assert.isAtLeast(projectsMin, projectsGoal);
		});

		it("should not attribute a project to an evaluator who's child is in", () => {
			sut.generateAttributions(response, { evaluatorsGoal, projectsGoal });
			assert.isFalse(ds1_evaluators[0].to_evaluate.some(attribution => attribution.project.students.includes(ds1_students[0])));
		});

		it("should redirect to the 'admin' path", () => {
			sut.generateAttributions(response, { evaluatorsGoal, projectsGoal });
			assert.strictEqual(response.redirectPath, Pathname.admin);
		});

		it("should not generate attributions for deactivated projects", () => {
			const project = new Project("p4", "n4", 4, Filiere.il, "", VideoType.hosted, false);
			projectRepository.create(project);

			sut.generateAttributions(response, { evaluatorsGoal, projectsGoal });

			assert.isFalse(evaluatorRepository.entities.reduce(function (acc: boolean, evaluator: Evaluator): boolean {
				return evaluator.to_evaluate.some(attribution => attribution.project === project) || acc;
			}, false));
		});
		
		it("should support filiere viewing wishes", () => {
			sut.generateAttributions(response, { evaluatorsGoal, projectsGoal });

			assert.isTrue(ds1_evaluators[0].to_evaluate.some(attribution => attribution.project.filiere === Filiere.asi));
			assert.isTrue(ds1_evaluators[0].to_evaluate.some(attribution => attribution.project.filiere === Filiere.il));
			assert.isTrue(ds1_evaluators[1].to_evaluate.some(attribution => attribution.project.filiere === Filiere.asi));
			assert.isTrue(ds1_evaluators[2].to_evaluate.some(attribution => attribution.project.filiere === Filiere.il));
			assert.isTrue(ds1_evaluators[4].to_evaluate.some(attribution => attribution.project.filiere === Filiere.asi));
			assert.isTrue(ds1_evaluators[4].to_evaluate.some(attribution => attribution.project.filiere === Filiere.il));
		});

		it("should not attribute undefined projects", () => {
			sut.generateAttributions(response, { evaluatorsGoal, projectsGoal });

			for (let evaluator of ds1_evaluators) {
				assert.isFalse(evaluator.to_evaluate.some(attribution => attribution.project === undefined));
			}
		});
	});

	describe("evaluateProject", () => {
		let form: IEvaluateForm;

		beforeEach(() => {
			form = {
				evaluator_id: evaluators[0].id,
				project_id: projects[0].id,
				grades: [4, 5],
				remarks: "",
			};

			evaluatorRepository.create(evaluators[0]);
			projectRepository.create(projects[0]);
			criteriaRepository.create(criterias[0]);
			criteriaRepository.create(criterias[1]);
		});

		it("should register the grades", () => {
			sut.evaluateProject(response, form);

			assert.isTrue(evaluators[0].grades.some(grade => grade.project === projects[0] && grade.criteria === criterias[0] && grade.value === 4));
			assert.isTrue(evaluators[0].grades.some(grade => grade.project === projects[0] && grade.criteria === criterias[1] && grade.value === 5));
		});

		it("should redirect to the 'home' path when the evaluator has not watched all its videos", () => {
			evaluators[0].registerProjectsToEvaluate([projects[1]], true);
			sut.evaluateProject(response, form);
			assert.strictEqual(response.redirectPath, Pathname.home);
		});

		it("should redirect to the 'home' path even when the evaluator has watched all its videos", () => {
			evaluators[0].registerProjectsToEvaluate([projects[0]], true);
			sut.evaluateProject(response, form);
			assert.strictEqual(response.redirectPath, Pathname.home);
		});
	});

	describe("editGrades", () => {
		let editForm: IEditGradesForm;
		let evaluateForm: IEvaluateForm;

		beforeEach(() => {
			evaluators[0] = new Evaluator("e1", "", "", EvaluatorType.other);

			editForm = {
				evaluator_id: evaluators[0].id,
				grades_id: [],
				grades: [4, 5],
				remarks: "",
			};

			evaluateForm = {
				evaluator_id: evaluators[0].id,
				project_id: projects[0].id,
				grades: [4, 5],
				remarks: "",
			};

			evaluatorRepository.create(evaluators[0]);
			projectRepository.create(projects[0]);
			criteriaRepository.create(criterias[0]);
			criteriaRepository.create(criterias[1]);
			sut.evaluateProject(response, evaluateForm);
			editForm.grades_id = gradeRepository.entities.map(grade => grade.id);
		});

		it("should register the grades", () => {
			editForm.grades = [1, 2];

			sut.editGrades(response, editForm);

			assert.isTrue(evaluators[0].grades.some(grade => grade.project === projects[0] && grade.criteria === criterias[0] && grade.value === 1));
			assert.isTrue(evaluators[0].grades.some(grade => grade.project === projects[0] && grade.criteria === criterias[1] && grade.value === 2));
		});

		it("should redirect to the 'home' path", () => {
			evaluators[0].registerProjectsToEvaluate([projects[1]], true);
			editForm.grades = [1, 2];

			sut.editGrades(response, editForm);

			assert.strictEqual(response.redirectPath, Pathname.home);
		});

		it("should redirect to the 'projects' path when the evaluator has watched all its videos", () => {
			evaluators[0].registerProjectsToEvaluate([projects[0]], true);
			editForm.grades = [1, 2];

			sut.editGrades(response, editForm);

			assert.strictEqual(response.redirectPath, Pathname.projects);
		});
	});
});
