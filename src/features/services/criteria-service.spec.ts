
// ===== Imports ===== //

import { assert } from "chai";
import "mocha";

import { ResponseManager } from "../req-res-manager.mock";
import { CriteriaRepository } from "../../stores/criteria-repository";
import { CriteriaService, ICriteriaForm } from "./criteria-service";
import { Pathname } from "../pathname";
import { Criteria } from "../../entities/criteria";

// ===== Tests ===== //

describe("CriteriaService", () => {
	let criteriaRepository: CriteriaRepository;
	let response: ResponseManager;
	let sut: CriteriaService;

	beforeEach(() => {
		criteriaRepository = new CriteriaRepository();
		response = new ResponseManager();
		sut = new CriteriaService(criteriaRepository);
	});

	describe("createCriteria", () => {
		const form: ICriteriaForm = {
			name: "criteria 1",
			description: "description",
		};

		it("should create a new criteria", () => {
			sut.createCriteria(response, form);

			const criteria = criteriaRepository.getByName(form.name);
			assert.isNotNull(criteria);
			assert.strictEqual((criteria as Criteria).name, form.name);
			assert.strictEqual((criteria as Criteria).description, form.description);
		});

		it("should redirect to the 'admin/criterias' pathname", () => {
			sut.createCriteria(response, form);
			assert.strictEqual(response.redirectPath, Pathname.adminCriterias);
		});
	});

	describe("updateCriteria", () => {
		let criteria: Criteria;
		let form: ICriteriaForm;

		beforeEach(() => {
			form = {
				id: "abc",
				name: "criteria 1",
				description: "description",
			};

			criteria = new Criteria(form.id as string, form.name, form.description);
			criteriaRepository.create(criteria);
		});

		it("should modify an existing criteria", () => {
			form.name = "criteria 1 (mod)";
			form.description = "description (mod)";

			sut.updateCriteria(response, form);

			assert.strictEqual(criteria.name, form.name);
			assert.strictEqual(criteria.description, form.description);
		});

		it("should redirect to the 'admin/criterias' pathname", () => {
			sut.updateCriteria(response, form);
			assert.strictEqual(response.redirectPath, Pathname.adminCriterias);
		});
	});

	describe("deleteCriteria", () => { });
});
