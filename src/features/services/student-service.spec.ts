
// ===== Imports ===== //

import { readFileSync } from "fs";
import { assert } from "chai";
import "mocha";

import { StudentService } from "./student-service";
import { ResponseManager } from "../req-res-manager.mock";
import { StudentRepository } from "../../stores/student-repository";
import { Student } from "../../entities/student";
import { Pathname } from "../pathname";
import { Filiere } from "../../entities/filiere";

// ===== Mocks and filler data ===== //

const studentsJSON = readFileSync("./src/test-data/eleves.json", "utf-8");

// ===== Tests ===== //

describe("StudentService", () => {
	let studentRepository: StudentRepository;
	let response: ResponseManager;
	let sut: StudentService;

	beforeEach(() => {
		studentRepository = new StudentRepository();
		response = new ResponseManager();
		sut = new StudentService(studentRepository);
	});

	describe("importStudents", () => {
		it("should parse the JSON data", () => {
			sut.importStudents(response, studentsJSON);

			const students: Student[] = studentRepository.entities;
			assert.strictEqual(students[0].firstname, "Alan");
			assert.strictEqual(students[0].lastname, "TURING");
			assert.strictEqual(students[0].email, "turing@et.intechinfo.fr");
			assert.strictEqual(students[0].semester, 1);
			assert.strictEqual(students[0].filiere, Filiere.none);

			assert.strictEqual(students[1].firstname, "Adda");
			assert.strictEqual(students[1].lastname, "LOVELACE");
			assert.strictEqual(students[1].email, "lovelace@et.intechinfo.fr");
			assert.strictEqual(students[1].semester, 5);
			assert.strictEqual(students[1].filiere, Filiere.asi);
		});

		it("should redirect to the 'admin' path", () => {
			sut.importStudents(response, studentsJSON);
			assert.strictEqual(response.redirectPath, Pathname.adminImports);
		});

		it("should only import students at a semester lower than 6 (e.i. 1 to 5)", () => {
			sut.importStudents(response, studentsJSON);

			const students: Student[] = studentRepository.entities;
			const steveJobs = students.find(student => student.email === "jobs@et.intechinfo.fr");
			assert.strictEqual(steveJobs, undefined);
		});

		it("should parse 'S10' as a semestre 10 and not 0", () => {
			sut.importStudents(response, studentsJSON);

			const students: Student[] = studentRepository.entities;
			const billGates = students.find(student => student.email === "gates@et.intechinfo.fr");
			assert.strictEqual(billGates, undefined);
		});
	});
});
