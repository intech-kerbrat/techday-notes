import { Student } from "../../entities/student";
import { IStudentRepository } from "../i-repository";
import { IResponseManager } from "../i-req-res-manager";
import { Pathname } from "../pathname";

export class StudentService {
	constructor(
		private studentRepository: IStudentRepository,
	) { }

	/**
	 * Import the students from a JSON file.
	 * @param studentsJSON The raw JSON string to import the students from.
	 */
	public importStudents(response: IResponseManager, studentsJSON: string): void {
		let students: any;

		try {
			students = JSON.parse(studentsJSON);
		} catch (e) {
			console.error(`Could not parse students.`);
			response.redirect(Pathname.admin);
		}

		for (let prop in students["ELEVES"]) {
			const studentData = students["ELEVES"][prop];
			const semester = Number((studentData["SEMESTRE"] as string).substring(1));

			if (semester >= 6) {
				continue;
			}

			const student = new Student(
				this.studentRepository.nextId(),
				studentData["PRENOM"],
				studentData["NOM"],
				studentData["EMAIL"],
				semester,
				Student.getFiliereFromString(studentData["FILIERE"]),
			);

			this.studentRepository.create(student);
		}

		response.redirect(Pathname.adminImports);
	}
}
