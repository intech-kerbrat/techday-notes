
// ===== Imports ===== //

import { assert } from "chai";
import "mocha";

import { AuthService } from "./auth-service";
import { EvaluatorRepository } from "../../stores/evaluator-repository";
import { Evaluator } from "../../entities/evaluator";
import { Pathname } from "../pathname";
import { RequestManager, ResponseManager } from "../req-res-manager.mock";
import { EvaluatorType } from "../../entities/evaluator-type";

// ===== Tests ===== //

describe("AuthService", () => {
	const token = "xxx";
	let evaluatorRepository: EvaluatorRepository;
	let request: RequestManager;
	let response: ResponseManager;
	let sut: AuthService;

	beforeEach(() => {
		evaluatorRepository = new EvaluatorRepository();
		request = new RequestManager();
		response = new ResponseManager();
		sut = new AuthService(evaluatorRepository);
	});

	describe("checkConnexionToken", () => {
		it("should set the evaluator instance on the request and instruct to go to the next middleware", () => {
			const evaluator = new Evaluator("a", "abc@site.com", "Abc SITE", EvaluatorType.parent);
			evaluator.authenticate(token);
			evaluatorRepository.create(evaluator);

			sut.checkConnexionToken(request, response, token);

			assert.strictEqual(request.evaluator, evaluator);
			assert.strictEqual(response.nextMiddleware, true);
		});

		it("should clear the cookie when it founds that the token is expired", () => {
			const evaluator = new Evaluator("a", "abc@site.com", "Abc SITE", EvaluatorType.parent);
			evaluator.authenticate(token);
			evaluator.tokenExpiry = Date.now() - 1e6;
			evaluatorRepository.create(evaluator);

			sut.checkConnexionToken(request, response, token);

			assert.strictEqual(response.clearedTokenName, "connexion_token");
		});

		it("should redirect an expired connexion token evaluator to the login web page", () => {
			const evaluator = new Evaluator("a", "abc@site.com", "Abc SITE", EvaluatorType.parent);
			evaluator.authenticate(token);
			evaluator.tokenExpiry = Date.now() - 1e6;
			evaluatorRepository.create(evaluator);

			sut.checkConnexionToken(request, response, token);

			assert.strictEqual(response.redirectPath, Pathname.login);
		});

		it("should redirect an non existing connexion token user to the login web page", () => {
			sut.checkConnexionToken(request, response, "a token that does not exist");
			assert.strictEqual(response.redirectPath, Pathname.login);
		});

		it("should redirect an non existing connexion token user to the login web page", () => {
			sut.checkConnexionToken(request, response, undefined);
			assert.strictEqual(response.redirectPath, Pathname.login);
		});
	});
});
