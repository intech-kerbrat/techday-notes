import { Evaluator } from "../../entities/evaluator";
import { Filiere } from "../../entities/filiere";
import { Project } from "../../entities/project";

export type EvaluatorFiliereNode = [Evaluator, Filiere];

export class AttributionService {
	private attributionsSoFar: Map<Project, Evaluator[]> = new Map();

	constructor(
		private evaluators: Evaluator[],
		private projects: Project[],
	) {
		projects.forEach(project => this.attributionsSoFar.set(project, []));
		evaluators.forEach(evaluator =>
			evaluator.to_evaluate.forEach(attribution => this.attributionsSoFar.get(attribution.project)?.push(evaluator))
		);
	}

	/**
	 * @param evaluator An evaluator.
	 * @returns The next project to attribute to the given evaluator.
	 */
	public findNextCompatibleProject([evaluator, filiere]: EvaluatorFiliereNode): Project {
		const projectsAvailable = this.projects.filter(project => evaluator.hasToEvaluate(project) === false);
		const projectsWithoutChildren = projectsAvailable.filter(project => evaluator.children === undefined || project.students.includes(evaluator.children) === false);
		const projectsOnlyForFiliere = projectsWithoutChildren.filter(project => filiere === Filiere.any ? true : project.filiere === filiere);

		const project = projectsOnlyForFiliere.length > 0
			? this.getLowestProjectAttribution(projectsOnlyForFiliere)
			: this.getLowestProjectAttribution(projectsWithoutChildren);
		this.setAttribution(evaluator, project);

		return project;
	}

	/**
	 * @param project A project.
	 * @returns The next evaluator to attribute to the given project.
	 */
	public findNextCompatibleEvaluator(project: Project): Evaluator {
		const evaluatorsAvailable = this.evaluators.filter(evaluator => evaluator.hasToEvaluate(project) === false);
		const evaluatorsWithoutChildren = evaluatorsAvailable.filter(evaluator => evaluator.children === undefined || project.students.includes(evaluator.children) === false);

		const evaluator = this.getLowestEvaluatorAttribution(evaluatorsWithoutChildren);
		this.setAttribution(evaluator, project);

		return evaluator;
	}

	/**
	 * @param projects A list of projects to search in.
	 * @returns The project which has been assigned the least to watch by evaluators.
	 */
	public getLowestProjectAttribution(projects: Project[]): Project {
		return this.getProjectsByLowestAttribution(projects).keys().next().value;
	}

	/**
	 * @param projects A list of projects to search in.
	 * @returns The minimum number of times a project has been assigned to watch by an evaluator.
	 */
	public getLowestProjectAttributionCount(projects: Project[]): number {
		return this.getProjectsByLowestAttribution(projects).values().next().value;
	}

	private getProjectsByLowestAttribution(projects: Project[]): Map<Project, number> {
		const projectsWithAttributionCount = projects.map((project): [Project, number] => {
			let count = 0;

			if (this.attributionsSoFar.has(project)) {
				count = (this.attributionsSoFar.get(project) as Evaluator[]).length;
			}

			return [project, count];
		}).sort((a, b) => a[1] - b[1]);

		return new Map(projectsWithAttributionCount);
	}

	/**
	 * 
	 * @param evaluators A list of evaluators to search in.
	 * @returns The evaluator which has been assigned the least projects to watch.
	 */
	public getLowestEvaluatorAttribution(evaluators: Evaluator[]): Evaluator {
		return this.getEvaluatorsByLowestAttribution(evaluators).keys().next().value;
	}

	/**
	 * @param evaluators A list of evaluators to search in.
	 * @returns The minimum number of times an evaluator has been assigned to watch a project.
	 */
	public getLowestEvaluatorAttributionCount(evaluators: Evaluator[]): number {
		return this.getEvaluatorsByLowestAttribution(evaluators).values().next().value;
	}

	private getEvaluatorsByLowestAttribution(evaluators: Evaluator[]): Map<Evaluator, number> {
		const evaluatorsWithAttributionCount = evaluators.map((evaluator): [Evaluator, number] => {
			return [evaluator, evaluator.to_evaluate.length];
		}).sort((a, b) => a[1] - b[1]);

		return new Map(evaluatorsWithAttributionCount);
	}

	private setAttribution(evaluator: Evaluator, project: Project): void {
		if (this.attributionsSoFar.has(project)) {
			this.attributionsSoFar.get(project)?.push(evaluator);
		} else {
			this.attributionsSoFar.set(project, [evaluator]);
		}
	}
}
