import { Evaluator } from "../../entities/evaluator";
import { Project } from "../../entities/project";
import { Student } from "../../entities/student";
import { ICSVProvider } from "../i-provider";
import { IEvaluatorRepository, IProjectRepository, IStudentRepository } from "../i-repository";
import { IResponseManager } from "../i-req-res-manager";
import { Pathname } from "../pathname";
import { ResponseManager } from "../req-res-manager.mock";

export interface IProjectForm {
	id?: string;
	name: string;
	semester: string;
	filiere: string;
	video_url?: string;
	video_type?: string;
}

export interface IToggleProjectParticipation {
	project_id: string;
}

export class ProjectService {
	constructor(
		private readonly csvProvider: ICSVProvider,
		private readonly evaluatorRepository: IEvaluatorRepository,
		private readonly projectRepository: IProjectRepository,
		private readonly studentRepository: IStudentRepository,
	) { }

	/**
	 * Import all projects in the server.
	 * @param response 
	 * @param projectsCSV The CSV file content of the projects.
	 */
	public importProjects(response: IResponseManager, projectsCSV: string): void {
		try {
			const projects: string[][] = this.csvProvider.parse(projectsCSV, ";");
			projects.splice(0, 1);
	
			for (let projectData of projects) {
				const id = this.projectRepository.nextId();
				const name = projectData[0];
				const semester = Number(projectData[1].substring(1));
				const filiere = Student.getFiliereFromString(projectData[2]);
				const project = new Project(id, name, semester, filiere);
	
				const students = projectData[3].split(",");
	
				for (let studentData of students) {
					const [firstname, lastname] = Student.parseFirstAndLastNames(studentData);
					let student = this.studentRepository.getByFirstLastName(firstname, lastname);
	
					if (student !== null) {
						project.addStudent(student);
					} else {
						console.warn(`Could not find student '${firstname}' '${lastname}' when adding to project ${project.name}.`);
					}
				}

				this.projectRepository.create(project);
			}
		} catch (e) {
			console.error(e);
		} finally {
			response.redirect(Pathname.adminImports);
		}
	}

	/**
	 * Update a project with the form's content.
	 * @param form The form's content.
	 */
	public updateProject(response: IResponseManager, form: IProjectForm): void {
		if (form.id === undefined) {
			// response.redirect(Pathname.adminCriterias);
			return;
		}

		const criterialOrNull = this.projectRepository.getById(form.id);
		const criteria = this.parseForm(form);

		if (criterialOrNull !== null) {
			this.projectRepository.update(form.id, criteria);
		}

		response.redirect(Pathname.adminProjects);
	}

	/**
	 * Activate of deactivate a project's participation to the Tech Day.
	 */
	public toggleProjectParticipation(response: ResponseManager, form: IToggleProjectParticipation) {
		const project = this.projectRepository.getById(form.project_id);

		if (project) {
			if (project.active) {
				this.projectRepository.deactivateProject(project);
			} else {
				this.projectRepository.activateProject(project);
			}
		}

		response.redirect(Pathname.adminProjects);
	}

	private parseForm(form: IProjectForm): Project {
		let semester = Number(form.semester);
		let filiere = Student.getFiliereFromString(form.filiere);
		return new Project(
			form.id as string,
			form.name,
			semester,
			filiere,
			form.video_url,
			Project.parseVideoType(form.video_type as string),
		);
	}

	/**
	 * @returns An evaluator array that must evaluate the given project.
	 */
	public getEvaluatorsForProject(project: Project): Evaluator[] {
		return this.evaluatorRepository.entities.filter(evaluator => evaluator.to_evaluate.some(attribution => attribution.project === project));
	}
}
