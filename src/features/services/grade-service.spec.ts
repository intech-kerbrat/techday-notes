
// ===== Imports ===== //

import { assert } from "chai";
import "mocha";

import { Criteria } from "../../entities/criteria";
import { Evaluator } from "../../entities/evaluator";
import { EvaluatorType } from "../../entities/evaluator-type";
import { Filiere } from "../../entities/filiere";
import { Grade } from "../../entities/grade";
import { Project } from "../../entities/project";
import { ProjectResult } from "../../entities/project-result";

import { IProjectRepository, IEvaluatorRepository, IGradeRepository, ICriteriaRepository } from "../i-repository";
import { GradeService } from "./grade-service";

import { EvaluatorRepository } from "../../stores/evaluator-repository";
import { GradeRepository } from "../../stores/grade-repository";
import { ProjectRepository } from "../../stores/project-repository";
import { CriteriaRepository } from "../../stores/criteria-repository";

// ===== Mocks and filler data ===== //

let criterias: Criteria[];
let evaluators: Evaluator[];
let grades: Grade[];
let projects: Project[];

describe("GradeService", () => {
	let criteriaRepository: ICriteriaRepository;
	let evaluatorRepository: IEvaluatorRepository;
	let gradeRepository: IGradeRepository;
	let projectRepository: IProjectRepository;
	let sut: GradeService;

	beforeEach(() => {
		criterias = [
			new Criteria("c1", "", ""),
			new Criteria("c2", "", ""),
		];

		evaluators = [
			new Evaluator("e1", "", "", EvaluatorType.parent),
			new Evaluator("e2", "", "", EvaluatorType.parent),
			new Evaluator("e3", "", "", EvaluatorType.parent),
		];

		projects = [
			new Project("p1", "", 1, Filiere.none),
			new Project("p2", "", 1, Filiere.none),
			new Project("p3", "", 4, Filiere.asi),
			new Project("p4", "", 4, Filiere.asi),
		];

		criteriaRepository = new CriteriaRepository();
		evaluatorRepository = new EvaluatorRepository();
		gradeRepository = new GradeRepository();
		projectRepository = new ProjectRepository();

		sut = new GradeService(criteriaRepository, evaluatorRepository, projectRepository);
	});

	describe("computeGradesForCategory", () => {
		beforeEach(() => {
			// Create attributions
			evaluators[0].registerProjectsToEvaluate([projects[0], projects[1]], true);
			evaluators[1].registerProjectsToEvaluate([projects[1], projects[2]], true);
			evaluators[2].registerProjectsToEvaluate([projects[2], projects[0]], true);

			// Create grades for each Evaluator
			grades = [
				// Evaluator 1
				new Grade("g1", projects[0], criterias[0], 3),
				new Grade("g2", projects[0], criterias[1], 2),
				new Grade("g3", projects[1], criterias[0], 5),
				new Grade("g4", projects[1], criterias[1], 4),

				// Evaluator 2
				new Grade("g5", projects[1], criterias[0], 1),
				new Grade("g6", projects[1], criterias[1], 2),
				new Grade("g7", projects[2], criterias[0], 5),
				new Grade("g8", projects[2], criterias[1], 5),

				// Evaluator 3
				new Grade("g9", projects[0], criterias[0], 5),
				new Grade("g10", projects[0], criterias[1], 5),
				new Grade("g11", projects[2], criterias[0], 4),
				new Grade("g12", projects[2], criterias[1], 5),
			];

			// Register grades in the evaluators
			grades.forEach((grade, index) => evaluators[Math.floor(index / 4)].setGrade(grade));

			// Register all entities in their repositories
			for (let criteria of criterias) criteriaRepository.create(criteria);
			for (let evaluator of evaluators) evaluatorRepository.create(evaluator);
			for (let project of projects) projectRepository.create(project);
		});

		it("should compute all the grades for a project", () => {
			const results: ProjectResult[] = sut.computeGradesForCategory(project => project.semester <= 2);

			// (3+2 + 5+5) / 20 = 7.5 / 10
			assert.strictEqual(results.find(result => result.project === projects[0])?.value, 7.5 / 10, `Results for project p1 not correct`);
			// (5+4 + 1+2) / 20 = 6 / 10
			assert.strictEqual(results.find(result => result.project === projects[1])?.value, 6 / 10, `Results for project p2 not correct`);
			assert.strictEqual(results.find(result => result.project === projects[2]), undefined, `Found unexpected results for project p3`);
		});

		it("should order results descending", () => {
			const results: ProjectResult[] = sut.computeGradesForCategory(project => true);
			assert.isTrue(results[0].value > results[1].value && results[1].value > results[2].value);
		});

		it("should take into account the weight on criterias", () => {
			criterias[1].weight = 2;

			const results: ProjectResult[] = sut.computeGradesForCategory(project => true);

			// (3+2*2 + 5+5*2) / (1+2)*2*5 = 22 / 30
			assert.strictEqual(results.find(result => result.project === projects[0])?.value, 22 / 30, `Results for project p1 not correct`);
			// (5+4*2 + 1+2*2) / (1+2)*2*5 = 18 / 30
			assert.strictEqual(results.find(result => result.project === projects[1])?.value, 18 / 30, `Results for project p2 not correct`);
			// (5+5*2 + 4+5*2) / (1+2)*2*5 = 29 / 30
			assert.strictEqual(results.find(result => result.project === projects[2])?.value, 29 / 30, `Found unexpected results for project p3`);
		});

		it("should include a result for projects who don't have grades yet", () => {
			const results: ProjectResult[] = sut.computeGradesForCategory(project => true);
			assert.strictEqual(results[3].project, projects[3]);
			assert.strictEqual(results[3].value, 0);
		});
	});
});
