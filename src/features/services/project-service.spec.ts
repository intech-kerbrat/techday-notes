
// ===== Imports ===== //

import { readFileSync } from "fs";
import { assert } from "chai";
import parse from "csv-parse/lib/sync";
import "mocha";

import { ResponseManager } from "../req-res-manager.mock";
import { IProjectForm, IToggleProjectParticipation, ProjectService } from "./project-service";
import { ProjectRepository } from "../../stores/project-repository";
import { Filiere } from "../../entities/filiere";
import { StudentRepository } from "../../stores/student-repository";
import { Student } from "../../entities/student";
import { ICSVProvider } from "../i-provider";
import { Pathname } from "../pathname";
import { Project } from "../../entities/project";
import { EvaluatorRepository } from "../../stores/evaluator-repository";
import { Evaluator } from "../../entities/evaluator";
import { EvaluatorType } from "../../entities/evaluator-type";

// ===== Mocks and filler data ===== //

class CSVProvider implements ICSVProvider {
	parse(value: string, separator: string = ","): any {
		return parse(value, { delimiter: separator });
	}
}

const projectsCSV = readFileSync("./src/test-data/projets.csv", "utf-8");

const students: Student[] = [
	new Student("a", "Alan", "TURING", "turing@et.intechinfo.fr", 1, Filiere.none),
	new Student("a", "Adda", "LOVELACE", "lovelace@et.intechinfo.fr", 4, Filiere.asi),
];

// ===== Tests ===== //

describe("ProjectService", () => {
	let csvProvider = new CSVProvider();
	let evaluatorRepository: EvaluatorRepository;
	let projectRepository: ProjectRepository;
	let studentRepository: StudentRepository;
	let response: ResponseManager;
	let sut: ProjectService;

	beforeEach(() => {
		evaluatorRepository = new EvaluatorRepository();
		projectRepository = new ProjectRepository();
		studentRepository = new StudentRepository();
		response = new ResponseManager();

		studentRepository.create(students[0]);
		studentRepository.create(students[1]);

		sut = new ProjectService(csvProvider, evaluatorRepository, projectRepository, studentRepository);
	});

	describe("importProjects", () => {
		it("should import the projects from the CSV data", () => {
			sut.importProjects(response, projectsCSV);

			const projects = projectRepository.entities;
			assert.strictEqual(projects[0].name, "Projet 1");
			assert.strictEqual(projects[0].semester, 1);
			assert.strictEqual(projects[0].filiere, Filiere.none);
			assert.strictEqual(projects[1].name, "Projet 2");
			assert.strictEqual(projects[1].semester, 4);
			assert.strictEqual(projects[1].filiere, Filiere.asi);
		});

		it("should add the students instances to the projects", () => {
			sut.importProjects(response, projectsCSV);

			const projects = projectRepository.entities;
			assert.isTrue(projects[0].students.includes(students[0]));
			assert.isTrue(projects[0].students.includes(students[1]));
			assert.isTrue(projects[1].students.includes(students[0]));
			assert.isTrue(projects[1].students.includes(students[1]));
		});

		it("should add the students even if the first and last names are swaped", () => {
			studentRepository.create(students[0]);
			studentRepository.create(students[1]);

			sut.importProjects(response, projectsCSV);

			const projects = projectRepository.entities;
			assert.isTrue(projects[0].students.includes(students[0]));
			assert.isTrue(projects[0].students.includes(students[1]));
			assert.isTrue(projects[1].students.includes(students[0]));
			assert.isTrue(projects[1].students.includes(students[1]));
		});

		it("should redirect to the 'admin' path", () => {
			sut.importProjects(response, projectsCSV);
			assert.strictEqual(response.redirectPath, Pathname.adminImports);
		});
	});

	describe("updateProject", () => {
		let project: Project;
		let form: IProjectForm;

		beforeEach(() => {
			form = {
				id: "abc",
				name: "project 1",
				semester: "1",
				filiere: "non",
				video_url: "video url",
				video_type: "hosted",
			};

			project = new Project(
				form.id as string,
				form.name,
				Number(form.semester),
				Student.getFiliereFromString(form.filiere),
			);

			projectRepository.create(project);
		});

		it("should modify an existing project", () => {
			form.name = "project 1 (mod)";
			form.semester = "2";
			form.filiere = "asi";

			sut.updateProject(response, form);

			assert.strictEqual(project.name, form.name);
			assert.strictEqual(project.semester, Number(form.semester));
			assert.strictEqual(project.filiere, form.filiere);
			assert.strictEqual(project.video_url, form.video_url);
			assert.strictEqual(project.video_type, form.video_type);
		});

		it("should redirect to the 'admin/projects' pathname", () => {
			sut.updateProject(response, form);
			assert.strictEqual(response.redirectPath, Pathname.adminProjects);
		});
	});

	describe("toggleProjectParticipation", () => {
		const project = new Project("p1", "n1", 1, Filiere.none);
		const form: IToggleProjectParticipation = {
			project_id: "p1",
		};

		beforeEach(() => {
			projectRepository.create(project);
		});

		it("should deactivate the project's participation", () => {
			sut.toggleProjectParticipation(response, form);
			assert.isFalse(project.active);
		});

		it("should activate the project's participation", () => {
			projectRepository.deactivateProject(project);
			sut.toggleProjectParticipation(response, form);
			assert.isTrue(project.active);
		});

		it("should redirect to the 'admin/projects' pathname", () => {
			sut.toggleProjectParticipation(response, form);
			assert.strictEqual(response.redirectPath, Pathname.adminProjects);
		});
	});

	describe("getEvaluatorsForProject", () => {
		it("should retrieve all evaluators assigned to evaluate the given project", () => {
			const evaluators = [
				new Evaluator("e1", "", "", EvaluatorType.other),
				new Evaluator("e2", "", "", EvaluatorType.other),
			];

			const projects = [
				new Project("p1", "", 1, Filiere.none),
			];

			evaluatorRepository.create(evaluators[0]);
			evaluatorRepository.create(evaluators[1]);
			projectRepository.create(projects[0]);
			evaluatorRepository.createAttribution(evaluators[0], projects[0], true);
			evaluatorRepository.createAttribution(evaluators[1], projects[0], true);

			const result: Evaluator[] = sut.getEvaluatorsForProject(projects[0]);

			assert.deepStrictEqual(result, evaluators);
		});
	});
});
