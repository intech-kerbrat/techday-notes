import { Grade } from "../../entities/grade";
import { Project } from "../../entities/project";
import { ProjectResult } from "../../entities/project-result";

import { ICriteriaRepository, IEvaluatorRepository, IProjectRepository } from "../i-repository";

export class GradeService {
	public constructor(
		private criteriaRepository: ICriteriaRepository,
		private evaluatorRepository: IEvaluatorRepository,
		private projectRepository: IProjectRepository,
	) { }

	public computeGradesForCategory(projectSelector: (project: Project) => boolean): ProjectResult[] {
		const criterias = this.criteriaRepository.entities;
		const evaluators = this.evaluatorRepository.entities;
		
		// Aggregate all projects according to the project selector and count how many each is evaluated.
		const projectAttributions: Map<Project, number> = new Map();

		for (let evaluator of evaluators) {
			for (let attribution of evaluator.to_evaluate) {
				if (projectSelector(attribution.project) === true) {
					// const count = projectAttributions.get(attribution.project) || 0;
					projectAttributions.set(attribution.project, 0);
				}
			}
		}

		// Add all grades together.
		const projectGradesSum: Map<Project, number> = new Map();
		
		for (let evaluator of evaluators) {
			for (let criteria of criterias) {
				for (let [project] of projectAttributions) {
					const grade: Grade | undefined = evaluator.grades.find(grade => grade.criteria === criteria && grade.project === project);

					if (grade === undefined) {
						continue;
					}

					let sum: number | undefined = projectGradesSum.get(project);

					if (sum === undefined) {
						sum = 0;
					}

					projectGradesSum.set(project, sum + grade.value * criteria.weight);
					const totalWeights = projectAttributions.get(project) || 0;
					projectAttributions.set(project, totalWeights + criteria.weight);
				}
			}
		}

		// Computing averages.
		const projectResults: ProjectResult[] = [];
		// const weights: number = criterias.reduce((sum, criteria) => sum + criteria.weight, 0);

		for (let [project, weights] of projectAttributions) {
			const sum: number | undefined = projectGradesSum.get(project);

			if (sum === undefined) {
				// console.error(`Count not find sum for project`);
				// console.error(project);
				continue;
			}

			const average = sum / (weights * 5);
			projectResults.push(new ProjectResult(project, average));
		}

		// Insert projects that were not included in attributions.
		for (let project of this.projectRepository.entities) {
			if (
				projectResults.some(result => result.project === project) === false
				&& projectSelector(project) === true
			) {
				projectResults.push(new ProjectResult(project, 0));
			}
		}

		projectResults.sort((a, b) => a.project.name === b.project.name ? 0 : a.project.name < b.project.name ? -1 : 1);
		projectResults.sort((a, b) => a.project.semester - b.project.semester);
		projectResults.sort((a, b) => b.value - a.value);

		return projectResults;
	}
}
