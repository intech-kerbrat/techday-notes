// ===== Imports ===== //

import { IRequestManager, IResponseManager } from "../i-req-res-manager";
import { IEvaluatorRepository } from "../i-repository";
import { Pathname } from "../pathname";

// ===== Service ===== //

export class AuthService {
	constructor(
		private evaluatorRepository: IEvaluatorRepository,
	) { }

	/**
	 * Check the connexion token against the evaluator.
	 * @param token The token found on the request (optional).
	 * @returns If the auth check was sucessful or the redirection to the web page to authenticate.
	 */
	checkConnexionToken(request: IRequestManager, response: IResponseManager, token: string | undefined): void {
		if (token === undefined) {
			response.redirect(Pathname.login);
			return;
		}

		const evaluator = this.evaluatorRepository.getByToken(token);

		if (evaluator === null) {
			response.redirect(Pathname.login);
			return;
		}

		if (evaluator.isConnected() === true) {
			request.setEvaluator(evaluator);
			response.next();
		} else {
			response.clearCookie("connexion_token");
			response.redirect(Pathname.login);
		}
	}
}
