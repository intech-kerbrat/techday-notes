import { Criteria } from "../../entities/criteria";
import { ICriteriaRepository } from "../i-repository";
import { IResponseManager } from "../i-req-res-manager";
import { Pathname } from "../pathname";

export interface ICriteriaForm {
	id?: string;
	name: string;
	description: string;
	weight?: string;
}

export class CriteriaService {
	constructor(
		private criteriaRepository: ICriteriaRepository,
	) { }

	public createCriteria(response: IResponseManager, form: ICriteriaForm): void {
		const criteria = this.parseForm(form);
		this.criteriaRepository.create(criteria);
		response.redirect(Pathname.adminCriterias);
	}

	public updateCriteria(response: IResponseManager, form: ICriteriaForm): void {
		if (form.id === undefined) {
			response.redirect(Pathname.adminCriterias);
			return;
		}

		const criterialOrNull = this.criteriaRepository.getById(form.id);
		const criteria = this.parseForm(form);

		if (criterialOrNull !== null) {
			this.criteriaRepository.update(form.id, criteria);
		}

		response.redirect(Pathname.adminCriterias);
	}

	public deleteCriteria(response: IResponseManager): void { }

	private parseForm(form: ICriteriaForm): Criteria {
		let id: string;
		let weight: number | undefined;

		id = (form.id ? form.id : this.criteriaRepository.nextId());
		
		if (form.weight !== undefined) {
			weight = Number(form.weight);
		}

		return new Criteria(id, form.name, form.description, weight);
	}
}
