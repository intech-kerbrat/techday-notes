import { ResponseType, WebPage } from "./web-page";

export interface IResult {
	responseType: ResponseType;
	webPage?: WebPage;
}
