import { Evaluator } from "../entities/evaluator";
import { Pathname } from "./pathname";
import { WebPage } from "./web-page";

export interface IResponseManager {
	/**
	 * Instruct the server to call the next middleware.
	 */
	next(): void;

	/**
	 * Redirects the user to the given pathname.
	 * @param path The pathname to redirect to.
	 */
	redirect(path: Pathname): void;

	/**
	 * Renders the given page.
	 * @param page The page to render.
	 */
	render(page: WebPage): void;

	/**
	 * Sets a cookie on the response.
	 */
	setCookie(name: string, value: string, maxAge: number): void;

	/**
	 * Clears the cookie in the response for subsequent requests.
	 * @param name Cookie's name.
	 */
	clearCookie(name: string): void;
}

export interface IRequestManager {
	/**
	 * Sets an evaluator instance to the request for further use in the middlewares.
	 * @param evaluator An instance of an evaluator.
	 */
	setEvaluator(evaluator: Evaluator): void;
}
