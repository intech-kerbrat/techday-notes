// All the pathnames the server listens to.

export enum Pathname {
	admin = "/admin",
	adminImports = "/admin/imports",
	adminCriterias = "/admin/criterias",
	adminEvaluators = "/admin/evaluators",
	adminProjects = "/admin/projects",

	home = "/",
	login = "/login",
	projects = "/projects",
}
