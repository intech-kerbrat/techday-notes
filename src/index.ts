
// ===== Requires ===== //

import express from "express";
import bodyParser from "body-parser";
import cookieParser from "cookie-parser";
import nunjucks from "nunjucks";

import { Request, Response, NextFunction } from "express";

import routes from "./server/routes";
import { setupFilters } from "./server/filters";

// ===== Config ===== //

const app = express();

// Templating engine
const env = nunjucks.configure("./src/server", {
	autoescape: true,
	express: app,
});

setupFilters(env);

// URL logger
app.use(function (req: Request, res: Response, next: NextFunction) {
	console.log(`${req.method} ${req.url}`);
	next();
});

// Middlewares and routes
app.use(express.static("./assets"));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(routes);

// Launching the server
const port = process.env.PORT || 3000;
app.listen(port, function () {
	console.log(`Server started on port ${port}`);
});
