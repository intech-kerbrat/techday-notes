import { Response } from "express";
import { Evaluator } from "../../entities/evaluator";
import { Project } from "../../entities/project";
import { Pathname } from "../../features/pathname";

import { IEditGradesForm, IEvaluateForm } from "../../features/services/evaluator-service";
import { WebPage } from "../../features/web-page";

import { IBaseController } from "../i-base-controller";
import { AppRequest, ResponseManager } from "../req-res-manager";

import services from "../services";

class ProjectsController implements IBaseController {
	constructor() {
		this.displayProject = this.displayProject.bind(this);
	}

	public displayProject(req: AppRequest, res: Response): void {
		if (typeof req.query.id !== "string") {
			this.displayAllVideos(req, res);
			return;
		}

		const criterias = services.criteriaRepository.entities;
		const evaluator = req._app?.evaluator as Evaluator;
		const project = services.projectRepository.getById(req.query.id);

		if (project === null) {
			console.error(`Project id was not found ${req.query.id}`);
			res.redirect(Pathname.home);
			return;
		}

		const hasToEvaluate: boolean = evaluator.hasToEvaluate(project);
		const displayNotes: boolean = hasToEvaluate;

		const context: any = {
			project,
			evaluator,
			criterias,
			displayNotes,
			remarks: evaluator.remarks.find(remarks => remarks.project === project)?.remark,
		};

		const grades = evaluator?.grades.filter(grade => grade.project === project);

		if (grades && grades.length > 0) {
			context.grades = grades;
		}

		res.render("projects/project.html", context);
	}

	private displayAllVideos(req: AppRequest, res: Response): void {
		const criterias = services.criteriaRepository.entities;
		const evaluator = req._app?.evaluator;
		const projects = services.projectRepository.getActiveProjects();
		let semester = Number(req.query.semestre);

		if (isNaN(semester) || semester < 1 || semester > 5) {
			semester = 1;
		}

		const context = {
			projects,
			hasGrades: function (project: Project) {
				return evaluator?.hasGradesForProject(project, criterias);
			},
			activeSemester: semester,
		};

		res.render("projects/all-videos.html", context);
	}

	public displayAllPublicVideos(req: AppRequest, res: Response): void {
		const projects = services.projectRepository.getActiveProjects();

		if (req.query.id) {
			const project = services.projectRepository.getById(req.query.id as string);

			if (project === null) {
				console.error(`Project id was not found ${req.query.id}`);
				res.redirect(Pathname.home);
				return;
			}

			const context: any = {
				project,
				displayNotes: false,
			};

			res.render("projects/project.html", context);
		} else {
			let semester = Number(req.query.semestre);

			if (isNaN(semester) || semester < 1 || semester > 5) {
				semester = 1;
			}

			const context = {
				projects,
				hasGrades: () => false,
				activeSemester: semester,
			};

			res.render("projects/all-public-videos.html", context);
		}
	}

	public evaluateProject(req: AppRequest, res: Response): void {
		const response = new ResponseManager(req, res, this);
		const form: IEvaluateForm = {
			evaluator_id: req.body.evaluator_id,
			project_id: req.body.project_id,
			grades: (req.body.grades as string[]).map(str => Number(str)),
			remarks: req.body.remarks,
		};

		services.evaluatorService.evaluateProject(response, form);
	}

	public editGrades(req: AppRequest, res: Response): void {
		const response = new ResponseManager(req, res, this);
		const form: IEditGradesForm = {
			evaluator_id: req.body.evaluator_id,
			grades_id: (req.body["grades-id"] as string[]),
			grades: (req.body.grades as string[]).map(str => Number(str)),
			remarks: req.body.remarks,
		};

		services.evaluatorService.editGrades(response, form);
	}

	public handleDisplays(page: WebPage, req: AppRequest, res: Response): void { }
}

export default new ProjectsController();
