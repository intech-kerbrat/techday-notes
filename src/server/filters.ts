import { Environment } from "nunjucks";

export function setupFilters(env: Environment): void {
	env.addFilter("filiere", function (str: string) {
		let result = "";
	
		switch (str) {
			case "il":
			case "asi":
				result = str.toUpperCase();
				break;
		}
	
		return result;
	});

	env.addFilter("filiereLong", function (str: string) {
		let result = "";
	
		switch (str) {
			case "il":
				result = "Ingénierie logicielle";
				break;
			case "asi":
				result = "Architecture des systèmes d'information";
				break;
		}
	
		return result;
	});

	env.addFilter("juryType", function (str: string) {
		let result = "";

		switch (str) {
			case "employee":
				result = "Employé";
				break;
			case "student":
				result = "Étudiant";
				break;
			case "parent":
				result = "Parent";
				break;
			case "partner":
				result = "Partenaire";
				break;
			case "alumni":
				result = "Alumni";
				break;
		}

		return result;
	});
}
