import { mkdirSync, readdirSync } from "fs";
import { join } from "path";
import { v4 } from "uuid";
import parse from "csv-parse/lib/sync";

import config from "./config.json";

import { AuthService } from "../features/services/auth-service";
import { EvaluatorService } from "../features/services/evaluator-service";
import { ICSVProvider, ITokenProvider } from "../features/i-provider";
import { ProjectService } from "../features/services/project-service";
import { StudentService } from "../features/services/student-service";

import { CriteriaJsonRepository } from "../stores/criteria-json-repository";
import { EvaluatorJsonRepository } from "../stores/evaluator-json-repository";
import { ProjectJsonRepository } from "../stores/project-json-repository";
import { StudentJsonRepository } from "../stores/student-json-repository";
import { CriteriaService } from "../features/services/criteria-service";
import { GradeJsonRepository } from "../stores/grade-json-repository";
import { GradeService } from "../features/services/grade-service";

class UUIDProvider implements ITokenProvider {
	getToken(): string {
		return v4();
	}
}

class CSVProvider implements ICSVProvider {
	parse(value: string, separator: string = ","): any {
		return parse(value, { delimiter: separator });
	}
}

const uuidProvider = new UUIDProvider();
const csvProvider = new CSVProvider();

createDataFolderIfItDoesntExists();
// NOTE tkerbrat: The order of the restores are important as entities depends on each others.
const studentRepository = StudentJsonRepository.restoreFromJson(join(config.storageDir, "students.json"));
const projectRepository = ProjectJsonRepository.restoreFromJson(join(config.storageDir, "projects.json"), studentRepository);
const criteriaRepository = CriteriaJsonRepository.restoreFromJson(join(config.storageDir, "criterias.json"));
const gradeRepository = GradeJsonRepository.restoreFromJson(join(config.storageDir, "grades.json"), criteriaRepository, projectRepository);
const evaluatorRepository = EvaluatorJsonRepository.restoreFromJson(join(config.storageDir, "evaluators.json"), gradeRepository, projectRepository, studentRepository);

export default {
	criteriaRepository,
	evaluatorRepository,
	gradeRepository,
	projectRepository,
	studentRepository,

	authService: new AuthService(evaluatorRepository),

	criteriaService: new CriteriaService(criteriaRepository),
	evaluatorService: new EvaluatorService(csvProvider, uuidProvider, criteriaRepository, evaluatorRepository, gradeRepository, projectRepository, studentRepository),
	gradeService: new GradeService(criteriaRepository, evaluatorRepository, projectRepository),
	projectService: new ProjectService(csvProvider, evaluatorRepository, projectRepository, studentRepository),
	studentService: new StudentService(studentRepository),
};



function createDataFolderIfItDoesntExists(): void {
	const topLevelRepositoriesAndFiles = readdirSync(".", "utf-8");

	if (topLevelRepositoriesAndFiles.includes(config.storageDir) === false) {
		mkdirSync(config.storageDir);
	}
}
