import { NextFunction, Request, Response } from "express";
import config from "./config.json";

import { Evaluator } from "../entities/evaluator";
import { IRequestManager, IResponseManager } from "../features/i-req-res-manager";
import { Pathname } from "../features/pathname";
import { WebPage } from "../features/web-page";
import { IBaseController } from "./i-base-controller";

export interface AppRequest extends Request {
	_app?: {
		evaluator?: Evaluator,
	};
}

export class RequestManager implements IRequestManager {
	public readonly request: AppRequest;

	constructor(request: Request) {
		this.request = request;
		this.request._app = new Object();
	}

	setEvaluator(evaluator: Evaluator): void {
		if (this.request._app !== undefined) {
			this.request._app.evaluator = evaluator;
		} else {
			throw new Error(`RequestManager's _app property was not initialized.`);
		}
	}
}

export class ResponseManager implements IResponseManager {
	constructor(
		public readonly request: AppRequest,
		public readonly response: Response,
		public readonly controller: IBaseController,
		public readonly nextFunction?: NextFunction,
	) { }

	public next(): void {
		if (this.nextFunction === undefined) {
			throw new Error(`nextFunction was not provided`);
		}

		this.nextFunction();
	}

	public redirect(path: Pathname): void {
		if (path.startsWith("/admin")) {
			this.response.redirect(config.adminPath + path);
		} else {
			console.log(path);
			this.response.redirect(path);
		}
	}

	public render(page: WebPage): void {
		this.controller.handleDisplays(page, this.request, this.response);
	}

	public setCookie(name: string, value: string, maxAge: number): void {
		this.response.cookie(name, value, { httpOnly: true, maxAge });
	}

	public clearCookie(name: string): void {
		this.response.clearCookie(name);
	}
}
