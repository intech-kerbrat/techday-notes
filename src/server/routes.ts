import { Router } from "express";
const routes = Router();
const adminRouter = Router();

import config from "./config.json";
import { Pathname } from "../features/pathname";

import adminController from "./admin/admin-controller";
import authController from "./auth-controller";
import landingController from "./landing/landing-controller";
import projectsController from "./projects/projects-controller";

adminRouter.get(Pathname.admin, adminController.displayAdminHome);

adminRouter.get(Pathname.adminImports, adminController.displayAdminImports);
adminRouter.post("/admin/import-projects", adminController.importProjects);
adminRouter.post("/admin/import-students", adminController.importStudents);
adminRouter.post("/admin/import-jurys", adminController.importJurys);
adminRouter.post("/admin/import-partners", adminController.importPartners);

adminRouter.get(Pathname.adminCriterias, adminController.displayAdminCriterias);
adminRouter.post("/admin/create-criteria", adminController.createCriteria);
adminRouter.get("/admin/update-criteria", adminController.displayUpdateCriteria);
adminRouter.post("/admin/update-criteria", adminController.updateCriteria);

adminRouter.get(Pathname.adminEvaluators, adminController.displayAdminJurys);
adminRouter.get("/admin/update-jury", adminController.displayUpdateJury);
adminRouter.post("/admin/add-jury-attribution", adminController.addJuryAttribution);
adminRouter.get("/admin/delete-jury-attribution", adminController.deleteJuryAttribution);
adminRouter.post("/admin/generate-attributions", adminController.generateAttributions);

adminRouter.get("/admin/projects", adminController.displayAdminProjects);
adminRouter.get("/admin/update-project", adminController.displayUpdateProject);
adminRouter.post("/admin/update-project", adminController.updateProject);
adminRouter.get("/admin/toggle-project", adminController.toggleProject);

// adminRouter.get("/admin/students", adminController.displayAdminStudents);

adminRouter.get("/admin/results", adminController.displayAdminResults);

routes.get("/", authController.checkAuthentication, landingController.displayHome);
routes.get("/login", landingController.displayLogin);
routes.post("/login", landingController.login);

routes.get("/videos", projectsController.displayAllPublicVideos);
routes.get("/projects", authController.checkAuthentication, projectsController.displayProject);
routes.post("/projects-evaluate", authController.checkAuthentication, projectsController.evaluateProject);
routes.post("/projects-edit", authController.checkAuthentication, projectsController.editGrades);

routes.use(config.adminPath, adminRouter);

export default routes;
