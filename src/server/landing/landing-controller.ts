import { Response } from "express";

import { AppRequest, ResponseManager } from "../req-res-manager";
import { IBaseController } from "../i-base-controller";
import { WebPage } from "../../features/web-page";

import services from "../services";
import { Project } from "../../entities/project";

class LandingController implements IBaseController {
	public constructor() {
		this.displayHome = this.displayHome.bind(this);
		this.displayLogin = this.displayLogin.bind(this);
		this.login = this.login.bind(this);
	}

	// GET "/"
	public displayHome(req: AppRequest, res: Response): void {
		const response = new ResponseManager(req, res, this);
		services.evaluatorService.displayHome(response, req._app?.evaluator);
	}

	// GET "/login"
	public displayLogin(req: AppRequest, res: Response): void {
		res.render("landing/login.html");
	}

	// POST "/login"
	public login(req: AppRequest, res: Response): void {
		const response = new ResponseManager(req, res, this);
		services.evaluatorService.login(response, req.body.email);
	}

	/**
	 * Display the given page.
	 * @param page The page to display.
	 * @param res Express's response object.
	 */
	public handleDisplays(page: WebPage, req: AppRequest, res: Response): void {
		switch (page) {
			case WebPage.mustWatch:
				this.displayMustWatch(req, res);
				break;
			// case WebPage.allVideos:
			// 	this.displayAllVideos(req, res);
			// 	break;
			case WebPage.emailNotFound:
				this.displayEmailNotFound(req, res);
				break;
			default:
				throw new Error(`Unexpected display: ${page}`);
		}
	}

	private displayMustWatch(req: AppRequest, res: Response): void {
		const criterias = services.criteriaRepository.entities;
		const evaluator = req._app?.evaluator;

		const context = {
			evaluator,
			hasGrades: evaluator?.to_evaluate.map(attribution => evaluator.hasGradesForProject(attribution.project, criterias)),
			hasWatchedAllVideos: evaluator?.hasWatchedAllVideos(criterias),
		};

		res.render("landing/must-watch.html", context);
	}

	private displayEmailNotFound(req: AppRequest, res: Response): void {
		const context = {
			emailNotFound: true,
		};

		console.log("not found login");
		res.render("landing/login.html", context);
	}
}

export default new LandingController();
