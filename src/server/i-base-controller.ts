import { Response } from "express";
import { WebPage } from "../features/web-page";
import { AppRequest } from "./req-res-manager";

export interface IBaseController {
	handleDisplays(page: WebPage, req: AppRequest, res: Response): void;
}
