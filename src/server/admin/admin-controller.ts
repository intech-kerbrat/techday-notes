import { readFileSync } from "fs";
import { NextFunction, Response } from "express";
import { Fields, IncomingForm } from "formidable";

import config from "../config.json";

import { ICriteriaForm } from "../../features/services/criteria-service";
import { WebPage } from "../../features/web-page";

import { IBaseController } from "../i-base-controller";
import { AppRequest, ResponseManager } from "../req-res-manager";
import services from "../services";
import { IProjectForm, IToggleProjectParticipation } from "../../features/services/project-service";
import { IAttributionForm, IGenerateAttributionForm } from "../../features/services/evaluator-service";
import { Filiere } from "../../entities/filiere";
import { Pathname } from "../../features/pathname";
import { Evaluator } from "../../entities/evaluator";
import { Grade } from "../../entities/grade";
import { Project } from "../../entities/project";

class AdminController implements IBaseController {

	// GET "/admin"
	public displayAdminHome(req: AppRequest, res: Response): void {
		const context = {
			evaluators_nb: `${services.evaluatorRepository.entities.length} évaluateur(s)`,
			projects_nb: `${services.projectRepository.entities.length} projet(s)`,
			students_nb: `${services.studentRepository.entities.length} étudiant(s)`,
			adminPath: config.adminPath,
		};

		res.render("admin/home.html", context);
	}

	//#region imports
	// GET "/admin/imports"
	public displayAdminImports(req: AppRequest, res: Response): void {
		const context = {
			adminPath: config.adminPath,
		};

		res.render("admin/imports.html", context);
	}

	// POST "/admin/import-jurys"
	public importJurys(req: AppRequest, res: Response, next: NextFunction): void {
		const response = new ResponseManager(req, res, this);

		const form = new IncomingForm();
		form.parse(req, function (err: any, fields: Fields, files: any) {
			console.log(err);
			if (err) {
				next(err);
				return;
			}

			const jurysCSV = readFileSync(files.jurys.path, "utf-8");
			services.evaluatorService.importJurys(response, jurysCSV);
		});
	}

	// POST "/admin/import-partners"
	public importPartners(req: AppRequest, res: Response, next: NextFunction): void {
		const response = new ResponseManager(req, res, this);

		const form = new IncomingForm();
		form.parse(req, function (err: any, fields: Fields, files: any) {
			console.log(err);
			if (err) {
				next(err);
				return;
			}

			const jurysCSV = readFileSync(files.partners.path, "utf-8");
			services.evaluatorService.importPartners(response, jurysCSV);
		});
	}

	// POST "/admin/import-students"
	public importStudents(req: AppRequest, res: Response, next: NextFunction): void {
		const response = new ResponseManager(req, res, this);

		const form = new IncomingForm();
		form.parse(req, function (err: any, fields: Fields, files: any) {
			if (err) {
				next(err);
				return;
			}

			const studentsJSON = readFileSync(files.students.path, "utf-8");
			services.studentService.importStudents(response, studentsJSON);
		});
	}

	// POST "/admin/import-projects"
	public importProjects(req: AppRequest, res: Response, next: NextFunction): void {
		const response = new ResponseManager(req, res, this);

		const form = new IncomingForm();
		form.parse(req, function (err: any, fields: Fields, files: any) {
			if (err) {
				next(err);
				return;
			}

			const projectsCSV = readFileSync(files.projects.path, "utf-8");
			services.projectService.importProjects(response, projectsCSV);
		});
	}
	//#endregion

	//#region criterias
	// GET "/admin/criterias"
	public displayAdminCriterias(req: AppRequest, res: Response): void {
		const context = {
			criterias: services.criteriaRepository.entities,
			adminPath: config.adminPath,
		};

		res.render("admin/criterias/criterias.html", context);
	}

	// POST "/admin/create-criteria"
	public createCriteria(req: AppRequest, res: Response): void {
		const response = new ResponseManager(req, res, this);
		const form: ICriteriaForm = {
			name: req.body.name,
			description: req.body.description,
			weight: req.body.weight,
		};

		services.criteriaService.createCriteria(response, form);
	}

	// GET "/admin/update-criteria"
	public displayUpdateCriteria(req: AppRequest, res: Response): void {
		const context = {
			criteria: services.criteriaRepository.getById(req.query.id as string),
			adminPath: config.adminPath,
		};

		res.render("admin/criterias/criteria-edit.html", context);
	}

	// POST "/admin/update-criteria"
	public updateCriteria(req: AppRequest, res: Response): void {
		const response = new ResponseManager(req, res, this);
		const form: ICriteriaForm = {
			id: req.body.id,
			name: req.body.name,
			description: req.body.description,
			weight: req.body.weight,
		};

		services.criteriaService.updateCriteria(response, form);
	}
	//#endregion

	//#region jurys
	// GET "/admin/jurys"
	public displayAdminJurys(req: AppRequest, res: Response): void {
		services.evaluatorRepository.entities.sort((a, b) => a.type === b.type ? 0 : a.type < b.type ? -1 : 1);

		const context = {
			jurys: services.evaluatorRepository.entities,
			adminPath: config.adminPath,
		};

		res.render("admin/jurys/jurys.html", context);
	}

	// GET "/admin/update-jury"
	public displayUpdateJury(req: AppRequest, res: Response): void {
		const jury = services.evaluatorRepository.getById(req.query.id as string);
		const projects = services.projectRepository.entities;
		const criterias = services.criteriaRepository.entities;

		const gradesByProject: {
			project: Project,
			grades: Grade[],
			remarks: {
				remark: string,
				project: Project,
			} | undefined;
		}[] = [];

		for (let project of projects) {
			if (jury?.hasToEvaluate(project)) {
				gradesByProject.push({
					project,
					grades: jury.grades.filter(grade => grade.project === project),
					remarks: jury.remarks.find(remark => remark.project === project),
				});
			}
		}

		const context = {
			jury,
			projects,
			criterias,
			adminPath: config.adminPath,
			gradesByProject,
		};

		res.render("admin/jurys/jury-edit.html", context);
	}

	// POST "/admin/add-jury-attribution"
	public addJuryAttribution(req: AppRequest, res: Response): void {
		const response = new ResponseManager(req, res, this);
		const form: IAttributionForm = {
			evaluator_id: req.body.evaluator_id,
			project_id: req.body.project_id,
		};

		services.evaluatorService.createAttribution(response, form);
	}

	// GET "/admin/delete-jury-attribution"
	public deleteJuryAttribution(req: AppRequest, res: Response): void {
		const response = new ResponseManager(req, res, this);
		const form: IAttributionForm = {
			evaluator_id: req.query.evaluator_id as string,
			project_id: req.query.project_id as string,
		};

		services.evaluatorService.deleteAttribution(response, form);
	}

	// POST "/admin/generate-attributions"
	public generateAttributions(req: AppRequest, res: Response): void {
		const response = new ResponseManager(req, res, this);
		const form: IGenerateAttributionForm = {
			evaluatorsGoal: Number(req.body.evaluators_goal),
			projectsGoal: Number(req.body.projects_goal),
		};

		services.evaluatorRepository.entities.sort(() => Math.random() - 0.5);
		services.projectRepository.entities.sort(() => Math.random() - 0.5);

		services.evaluatorService.generateAttributions(response, form);
	}
	//#endregion

	//#region projects
	// GET "/admin/projects"
	public displayAdminProjects(req: AppRequest, res: Response): void {
		const projects = services.projectRepository.repository.entities;
		projects.sort((a, b) => a.filiere === b.filiere ? 0 : a.filiere < b.filiere ? -1 : 1);
		projects.sort((a, b) => a.semester - b.semester);

		const context = {
			projects,
			evaluators: projects.map(project => services.projectService.getEvaluatorsForProject(project)),
			adminPath: config.adminPath,
		};

		res.render("admin/projects/projects.html", context);
	}

	// GET "/admin/update-project"
	public displayUpdateProject(req: AppRequest, res: Response): void {
		const criterias = services.criteriaRepository.entities;
		const project = services.projectRepository.getById(req.query.id as string);

		if (project === null) {
			res.redirect(config.adminPath + Pathname.adminProjects);
			return;
		}

		const evaluators = services.evaluatorRepository.entities;
		const gradesByEvaluator: {
			evaluator: Evaluator,
			grades: Grade[],
			remarks: {
				remark: string,
				project: Project,
			} | undefined;
		}[] = [];

		for (let evaluator of evaluators) {
			if (evaluator.hasToEvaluate(project)) {
				gradesByEvaluator.push({
					evaluator,
					grades: evaluator.grades.filter(grade => grade.project === project),
					remarks: evaluator.remarks.find(remark => remark.project === project),
				});
			}
		}

		const context = {
			project,
			adminPath: config.adminPath,
			criterias,
			gradesByEvaluator,
		};

		res.render("admin/projects/project-edit.html", context);
	}

	// POST "/admin/update-project"
	public updateProject(req: AppRequest, res: Response): void {
		const response = new ResponseManager(req, res, this);
		const form: IProjectForm = {
			id: req.body.id,
			name: req.body.name,
			semester: req.body.semester,
			filiere: req.body.filiere,
		};

		if (req.body.video_url) form.video_url = req.body.video_url;
		if (req.body.video_type) form.video_type = req.body.video_type;

		services.projectService.updateProject(response, form);
	}

	// GET "/admin/toggle-project"
	public toggleProject(req: AppRequest, res: Response): void {
		const response = new ResponseManager(req, res, this);
		const form: IToggleProjectParticipation = {
			project_id: (req.query.id as string),
		};

		services.projectService.toggleProjectParticipation(response, form);
	}
	//#endregion

	// GET "/admin/students"
	public displayAdminStudents(req: AppRequest, res: Response): void {
		const context = {
			adminPath: config.adminPath,
		};

		res.render("admin/imports.html", context);
	}

	// GET "/admin/results"
	public displayAdminResults(req: AppRequest, res: Response): void {
		const context = {
			results1A: services.gradeService.computeGradesForCategory(project => project.semester <= 2),
			resultsIL: services.gradeService.computeGradesForCategory(project => project.filiere === Filiere.il),
			resultsSR: services.gradeService.computeGradesForCategory(project => project.filiere === Filiere.asi),
			adminPath: config.adminPath,
		};

		res.render("admin/results.html", context);
	}

	public handleDisplays(page: WebPage, req: AppRequest, res: Response): void { }
}

export default new AdminController();
