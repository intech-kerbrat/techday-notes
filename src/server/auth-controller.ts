import { NextFunction, Request, Response } from "express";

import { WebPage } from "../features/web-page";
import { IBaseController } from "./i-base-controller";
import { AppRequest, RequestManager, ResponseManager } from "./req-res-manager";

import services from "./services";

class AuthController implements IBaseController {
	constructor() {
		this.checkAuthentication = this.checkAuthentication.bind(this);
	}

	public handleDisplays(page: WebPage, req: AppRequest, res: Response): void { }

	public checkAuthentication(req: Request, res: Response, next: NextFunction): void {
		const request = new RequestManager(req);
		const response = new ResponseManager(req, res, this, next);
		const token: string | undefined = req.cookies["connexion_token"];

		services.authService.checkConnexionToken(request, response, token);
	}
}

export default new AuthController();
