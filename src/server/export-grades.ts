import { join } from "path";
import { EOL } from "os";
import { writeFileSync, createWriteStream } from "fs";
import { Workbook } from "exceljs";
import config from "./config.json";

import { CriteriaJsonRepository } from "../stores/criteria-json-repository";
import { EvaluatorJsonRepository } from "../stores/evaluator-json-repository";
import { GradeJsonRepository } from "../stores/grade-json-repository";
import { ProjectJsonRepository } from "../stores/project-json-repository";
import { StudentJsonRepository } from "../stores/student-json-repository";

const studentRepository = StudentJsonRepository.restoreFromJson(join(config.storageDir, "students.json"));
const projectRepository = ProjectJsonRepository.restoreFromJson(join(config.storageDir, "projects.json"), studentRepository);
const criteriaRepository = CriteriaJsonRepository.restoreFromJson(join(config.storageDir, "criterias.json"));
const gradeRepository = GradeJsonRepository.restoreFromJson(join(config.storageDir, "grades.json"), criteriaRepository, projectRepository);
const evaluatorRepository = EvaluatorJsonRepository.restoreFromJson(join(config.storageDir, "evaluators.json"), gradeRepository, projectRepository, studentRepository);

const outputPath = process.argv[2];

for (let project of projectRepository.entities) {
	const evaluators = evaluatorRepository.entities.filter(evaluator => evaluator.hasToEvaluate(project));
	const results: {
		evaluatorType: string,
		criteria1Note: string,
		criteria2Note: string,
		criteria3Note: string,
		remarks: string,
	}[] = [{
		evaluatorType: `"Jury"`,
		criteria1Note: '"' + criteriaRepository.entities[0].name + '"',
		criteria2Note: '"' + criteriaRepository.entities[1].name + '"',
		criteria3Note: '"' + criteriaRepository.entities[2].name + '"',
		remarks: `"Remarques"`,
	}];

	const book = new Workbook();
	const sheet = book.addWorksheet("Résultats");
	sheet.columns = [
		{ header: "Jury", key: "jury", width: 10 },
		{ header: criteriaRepository.entities[0].name, key: "crit1", width: 10 },
		{ header: criteriaRepository.entities[1].name, key: "crit2", width: 10 },
		{ header: criteriaRepository.entities[2].name, key: "crit3", width: 10 },
		{ header: "Remarques", key: "remarques", width: 10 },
	];

	for (let evaluator of evaluators) {
		const grades = evaluator.grades.filter(grade => grade.project === project);
		const remark = evaluator.remarks.find(remark => remark.project === project);

		if (grades.length >= 3) {
			results.push({
				evaluatorType: '"' + evaluator.type.toString() + '"',
				criteria1Note: '"' + grades[0].value.toString() + '"',
				criteria2Note: '"' + grades[1].value.toString() + '"',
				criteria3Note: '"' + grades[2].value.toString() + '"',
				remarks: '"' + remark?.remark as string + '"',
			});

			sheet.addRow({
				jury: evaluator.type.toString(),
				crit1: grades[0].value,
				crit2: grades[1].value,
				crit3: grades[2].value,
				remarques: remark?.remark,
			});

			// sheet.addRow([
			// 	evaluator.type.toString(),
			// 	grades[0].value,
			// 	grades[1].value,
			// 	grades[2].value,
			// 	remark?.remark,
			// ]);
		}
	}

	const filename: string = [
		`S${project.semester}-`,
		project.name.replace(/ /gi, "_"),
		`.xlsx`
	].join("");

	const contents: string = results.map(result => [
		result.evaluatorType,
		result.criteria1Note,
		result.criteria2Note,
		result.criteria3Note,
		result.remarks
	].join(";")).join(EOL);

	// writeFileSync(join(outputPath, filename), contents);
	(async function () {
		const stream = createWriteStream(filename);
		await book.xlsx.write(stream);
		console.log(book);
	})();
	break;
}
