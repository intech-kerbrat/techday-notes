import { Student } from "../entities/student";
import { IStudentRepository } from "../features/i-repository";
import { BaseRepository } from "./base-repository";

export class StudentRepository extends BaseRepository<Student> implements IStudentRepository {
	// IRepository<T>
	public update(id: string, values: Student): void {
		throw new Error("Method not implemented.");
	}

	// IStudentRepository
	getByFirstLastName(firstname: string, lastname: string): Student | null {
		const collator = new Intl.Collator('fr', { usage: 'search', sensitivity: 'base' });
		const fn = firstname;
		const ln = lastname;

		return this._getBy(student => {
			const sfn = student.firstname;
			const sln = student.lastname;

			return (collator.compare(sfn, fn) === 0 && collator.compare(sln, ln) === 0)
				|| (collator.compare(sfn, ln) === 0 && collator.compare(sln, fn) === 0);
		});
	}
}
