import { writeFileSync } from "fs";

import { Evaluator } from "../entities/evaluator";
import { EvaluatorType } from "../entities/evaluator-type";
import { IEvaluatorRepository } from "../features/i-repository";
import { BaseJSONRepository } from "./base-json-repository";
import { Entity } from "../entities/entity";
import { EvaluatorRepository } from "./evaluator-repository";
import { tryReadOrReset } from "./restore-data";
import { StudentJsonRepository } from "./student-json-repository";
import { EvaluationAttribution } from "../entities/evaluation-attribution";
import { ProjectJsonRepository } from "./project-json-repository";
import { Project } from "../entities/project";
import { GradeJsonRepository } from "./grade-json-repository";
import { Filiere } from "../entities/filiere";

interface EvaluatorDTO {
	id: string,
	email: string;
	name: string;
	type: string;
	children: Entity | null;
	connexionToken?: string;
	tokenExpiry?: number;
	to_evaluate: {
		project: Entity,
		manual: boolean,
	}[];
	grades: Entity[];
	remarks: { remark: string, project: Entity }[];
	viewingWishes: string[];
}

export class EvaluatorJsonRepository extends BaseJSONRepository<Evaluator> implements IEvaluatorRepository {
	public repository: EvaluatorRepository = new EvaluatorRepository();

	private constructor(savePath: string) {
		super(savePath);
	}

	public static restoreFromJson(
		path: string,
		gradesRepository: GradeJsonRepository,
		projectRepository: ProjectJsonRepository,
		studentRepository: StudentJsonRepository
	): EvaluatorJsonRepository {
		const repository = new EvaluatorJsonRepository(path);
		const evaluators: EvaluatorDTO[] = tryReadOrReset(path);

		for (let evaluatorData of evaluators) {
			const evaluator = new Evaluator(
				evaluatorData.id,
				evaluatorData.email,
				evaluatorData.name,
				evaluatorData.type as EvaluatorType,
				evaluatorData.connexionToken,
				evaluatorData.tokenExpiry,
			);

			if (evaluatorData.children !== null) {
				const student = studentRepository.getById(evaluatorData.children.id);

				if (student !== null) {
					evaluator.setChild(student);
				}
			}

			for (let attribution of evaluatorData.to_evaluate) {
				const project = projectRepository.getById(attribution.project.id);

				if (project) {
					evaluator.to_evaluate.push(new EvaluationAttribution(project, attribution.manual));
				}
			}

			for (let gradeData of evaluatorData.grades) {
				const grade = gradesRepository.getById(gradeData.id);

				if (grade) {
					evaluator.grades.push(grade);
				}
			}

			for (let remarksData of evaluatorData.remarks) {
				const project = projectRepository.getById(remarksData.project.id);

				if (project) {
					evaluator.remarks.push({
						project: project,
						remark: remarksData.remark,
					});
				}
			}

			evaluator.viewingWishes = evaluatorData.viewingWishes as Filiere[];

			repository.repository.create(evaluator);
		}

		return repository;
	}

	public serialize(): void {
		const evaluatorsDTOs: EvaluatorDTO[] = [];

		for (let evaluator of this.repository.entities) {
			const evaluatorDTO: EvaluatorDTO = {
				id: evaluator.id,
				email: evaluator.email,
				name: evaluator.name,
				type: evaluator.type,
				children: null,
				to_evaluate: [],
				grades: [],
				remarks: [],
				viewingWishes: evaluator.viewingWishes,
			};

			if (evaluator.connexionToken) evaluatorDTO.connexionToken = evaluator.connexionToken;
			if (evaluator.tokenExpiry) evaluatorDTO.tokenExpiry = evaluator.tokenExpiry;

			if (evaluator.children !== undefined) {
				evaluatorDTO.children = {
					id: evaluator.children.id,
				};
			}

			for (let attribution of evaluator.to_evaluate) {
				evaluatorDTO.to_evaluate.push({
					project: { id: attribution.project.id },
					manual: attribution.manual,
				});
			}

			for (let grade of evaluator.grades) {
				evaluatorDTO.grades.push({ id: grade.id });
			}

			for (let remark of evaluator.remarks) {
				evaluatorDTO.remarks.push({
					project: { id: remark.project.id },
					remark: remark.remark,
				});
			}

			evaluatorsDTOs.push(evaluatorDTO);
		}

		const content = JSON.stringify(evaluatorsDTOs);
		writeFileSync(this.savePath, content, "utf-8");
	}

	public update(id: string, values: Evaluator): void {
		this.serialize();
	}

	//#region IEvaluatorRepository
	public getByEmail(email: string): Evaluator | null {
		return this.repository.getByEmail(email);
	}

	public getByToken(token: string): Evaluator | null {
		return this.repository.getByToken(token);
	}

	public createAttribution(evaluator: Evaluator, project: Project, manual: boolean): void {
		this.repository.createAttribution(evaluator, project, manual);
		this.serialize();
	}

	public deleteAttribution(evaluator: Evaluator, project: Project): void {
		this.repository.deleteAttribution(evaluator, project);
		this.serialize();
	}

	public authenticate(connexionToken: string, tokenExpiry: number): void {
		this.serialize();
	}
	//#endregion
}
