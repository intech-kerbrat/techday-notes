import { Criteria } from "../entities/criteria";
import { ICriteriaRepository } from "../features/i-repository";
import { BaseRepository } from "./base-repository";

export class CriteriaRepository extends BaseRepository<Criteria> implements ICriteriaRepository {
	// IRepository<T>
	public update(id: string, values: Criteria): void {
		const criteria = this.getById(id);
		
		if (criteria !== null) {
			criteria.name = values.name;
			criteria.description = values.description;
			criteria.weight = values.weight;
		}
	}

	// ICriteriaRepository
	public getByName(name: string): Criteria | null {
		return this._getBy(criteria => criteria.name === name);
	}
}
