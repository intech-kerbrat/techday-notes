import { Project } from "../entities/project";
import { IProjectRepository } from "../features/i-repository";
import { BaseRepository } from "./base-repository";

export class ProjectRepository extends BaseRepository<Project> implements IProjectRepository {
	// IRepository<T>
	public update(id: string, values: Project): void {
		const project = this.getById(id);

		if (project !== null) {
			project.name = values.name;
			project.semester = values.semester;
			project.filiere = values.filiere;
			if (values.video_url) project.video_url = values.video_url;
			if (values.video_type) project.video_type = values.video_type;
		}
	}

	// IProjectRepository
	public getActiveProjects(): Project[] {
		return this.entities.filter(project => project.active);
	}

	public activateProject(project: Project): void {
		project.active = true;
	}

	public deactivateProject(project: Project): void {
		project.active = false;
	}
}
