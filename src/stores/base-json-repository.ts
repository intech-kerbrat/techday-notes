import { IRepository } from "../features/i-repository";

export abstract class BaseJSONRepository<T> implements IRepository<T> {
	public abstract repository: IRepository<T>;

	constructor(
		public savePath: string,
	) { }

	public abstract serialize(): void;

	public get entities(): T[] {
		return this.repository.entities;
	}

	public nextId(): string {
		return this.repository.nextId();
	}

	public create(entity: T): void {
		this.repository.create(entity);
		this.serialize();
	}

	public update(id: string, values: T): void {
		this.repository.update(id, values);
		this.serialize();
	}

	public getById(id: string): T | null {
		return this.repository.getById(id);
	}
}
