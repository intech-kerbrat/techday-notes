
// ===== Imports ===== //

import { assert } from "chai";
import "mocha";
import { Filiere } from "../entities/filiere";
import { Student } from "../entities/student";

import { StudentRepository } from "./student-repository";



// ===== Tests ===== //

describe("StudentRepository", () => {
	let studentRepository: StudentRepository;

	beforeEach(() => {
		studentRepository = new StudentRepository();
	})

	describe("getByFirstLastName", () => {
		it("should find a student regardless of case or accents", () => {
			const student = new Student("1", "Évariste", "Galois", "galois@et.intechinfo.fr", 10, Filiere.il);
			studentRepository.create(student);

			const result = studentRepository.getByFirstLastName("evariste", "GALOIS");

			assert.isNotNull(result);
			assert.strictEqual(result?.email, student.email);
		});
	})
});
