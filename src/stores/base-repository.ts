import { v4 } from "uuid";

import { Entity } from "../entities/entity";
import { IRepository } from "../features/i-repository";

export abstract class BaseRepository<T extends Entity> implements IRepository<T> {
	public entities: T[] = [];

	public _getBy(predicate: (entity: T) => boolean): T | null {
		let match = null;

		for (let entity of this.entities) {
			if (predicate(entity)) {
				match = entity;
				break;
			}
		}

		return match;
	}

	public nextId(): string {
		return v4();
	}

	public create(entity: T): void {
		if (this.entities.includes(entity) === false) {
			this.entities.push(entity);
		}
	}

	public abstract update(id: string, values: T): void;

	public getById(id: string): T | null {
		return this._getBy(entity => entity.id === id) || null;
	}
}
