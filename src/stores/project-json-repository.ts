import { writeFileSync } from "fs";

import { Entity } from "../entities/entity";
import { Project } from "../entities/project";
import { Student } from "../entities/student";

import { IProjectRepository } from "../features/i-repository";

import { BaseJSONRepository } from "./base-json-repository";
import { ProjectRepository } from "./project-repository";
import { StudentJsonRepository } from "./student-json-repository";
import { tryReadOrReset } from "./restore-data";

interface ProjectDTO {
	id: string;
	name: string;
	semester: number;
	filiere: string;
	students: Entity[];
	video_url?: string;
	video_type?: string;
	active: boolean;
}

export class ProjectJsonRepository extends BaseJSONRepository<Project> implements IProjectRepository {
	public repository: ProjectRepository = new ProjectRepository();

	private constructor(savePath: string) {
		super(savePath);
	}

	public static restoreFromJson(path: string, studentRepository: StudentJsonRepository): ProjectJsonRepository {
		const repository = new ProjectJsonRepository(path);
		const projects: ProjectDTO[] = tryReadOrReset(path);

		for (let projectData of projects) {
			const project = new Project(
				projectData.id,
				projectData.name,
				projectData.semester,
				Student.getFiliereFromString(projectData.filiere),
				projectData.video_url,
				(typeof projectData.video_type === "string" ? Project.parseVideoType(projectData.video_type) : undefined),
				projectData.active,
			);

			for (let studentId of projectData.students) {
				const student = studentRepository.getById(studentId.id);

				if (student !== null) {
					project.addStudent(student);
				}
			}

			repository.repository.create(project);
		}

		return repository;
	}

	public serialize(): void {
		const projectsDTOs: ProjectDTO[] = [];

		for (let project of this.repository.entities) {
			const projectDTO: ProjectDTO = {
				id: project.id,
				name: project.name,
				semester: project.semester,
				filiere: project.filiere,
				students: project.students.map(student => ({ id: student.id })),
				video_url: project.video_url,
				video_type: project.video_type,
				active: project.active,
			};

			projectsDTOs.push(projectDTO);
		}

		const content = JSON.stringify(projectsDTOs);
		writeFileSync(this.savePath, content, "utf-8");
	}

	// IProjectRepository
	public getActiveProjects(): Project[] {
		return this.repository.getActiveProjects();
	}

	public activateProject(project: Project): void {
		this.repository.activateProject(project);
		this.serialize();
	}

	public deactivateProject(project: Project): void {
		this.repository.deactivateProject(project);
		this.serialize();
	}
}
