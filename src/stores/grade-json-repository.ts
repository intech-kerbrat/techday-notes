import { writeFileSync } from "fs";
import { Entity } from "../entities/entity";
import { Evaluator } from "../entities/evaluator";

import { Grade } from "../entities/grade";
import { IGradeRepository } from "../features/i-repository";
import { BaseJSONRepository } from "./base-json-repository";
import { CriteriaJsonRepository } from "./criteria-json-repository";
import { GradeRepository } from "./grade-repository";
import { ProjectJsonRepository } from "./project-json-repository";
import { tryReadOrReset } from "./restore-data";

interface GradeDTO {
	id: string;
	criteria: Entity;
	project: Entity;
	value: number;
}

export class GradeJsonRepository extends BaseJSONRepository<Grade> implements IGradeRepository {
	public repository: GradeRepository = new GradeRepository();

	private constructor(savePath: string) {
		super(savePath);
	}

	public static restoreFromJson(
		path: string,
		criteriaRepository: CriteriaJsonRepository,
		projectRepository: ProjectJsonRepository
	): GradeJsonRepository {
		const repository = new GradeJsonRepository(path);
		const grades: GradeDTO[] = tryReadOrReset(path);

		for (let gradeData of grades) {
			const project = projectRepository.getById(gradeData.project.id);
			const criteria = criteriaRepository.getById(gradeData.criteria.id);

			if (project == null || criteria == null) {
				console.warn(`Grade ${gradeData.id} not added. Did not found project ${gradeData.project.id} or criteria ${gradeData.criteria.id}`);
				continue;
			}

			const grade = new Grade(
				gradeData.id,
				project,
				criteria,
				gradeData.value,
			);

			repository.create(grade);
		}

		return repository;
	}

	public serialize(): void {
		const gradesDTOs: GradeDTO[] = [];

		for (let grade of this.repository.entities) {
			const gradeDTO: GradeDTO = {
				id: grade.id,
				criteria: { id: grade.criteria.id },
				project: { id: grade.project.id },
				value: grade.value,
			};

			gradesDTOs.push(gradeDTO);
		}

		const content = JSON.stringify(gradesDTOs);
		writeFileSync(this.savePath, content, "utf-8");
	}

	// IGradeRepository
}
