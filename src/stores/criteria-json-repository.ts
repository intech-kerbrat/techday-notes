import { writeFileSync } from "fs";

import { Criteria } from "../entities/criteria";
import { ICriteriaRepository } from "../features/i-repository";
import { BaseJSONRepository } from "./base-json-repository";
import { CriteriaRepository } from "./criteria-repository";
import { tryReadOrReset } from "./restore-data";

export class CriteriaJsonRepository extends BaseJSONRepository<Criteria> implements ICriteriaRepository {
	public repository: CriteriaRepository = new CriteriaRepository();

	private constructor(savePath: string) {
		super(savePath);
	}

	public static restoreFromJson(path: string): CriteriaJsonRepository {
		const repository = new CriteriaJsonRepository(path);
		let criterias: Criteria[] = tryReadOrReset<Criteria>(path);

		for (let criteria of criterias) {
			repository.repository.create(new Criteria(
				criteria.id,
				criteria.name,
				criteria.description,
				criteria.weight
			));
		}

		return repository;
	}

	public serialize(): void {
		const content = JSON.stringify(this.repository.entities);
		writeFileSync(this.savePath, content, "utf-8");
	}

	//#region ICriteriaRepository
	public getByName(name: string): Criteria | null {
		return this.repository.getByName(name);
	}
	//#endregion
}
