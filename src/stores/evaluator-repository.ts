import { Evaluator } from "../entities/evaluator";
import { Project } from "../entities/project";
import { IEvaluatorRepository } from "../features/i-repository";
import { BaseRepository } from "./base-repository";

export class EvaluatorRepository extends BaseRepository<Evaluator> implements IEvaluatorRepository {
	// IRepository<T>
	public update(id: string, values: Evaluator): void { }

	// IEvaluatorRepository
	public getByEmail(email: string): Evaluator | null {
		return this._getBy(evaluator => evaluator.email.toLowerCase() === email.toLowerCase());
	}

	public getByToken(token: string): Evaluator | null {
		return this._getBy(evaluator => evaluator.connexionToken === token);
	}

	public createAttribution(evaluator: Evaluator, project: Project, manual: boolean): void {
		evaluator.registerProjectsToEvaluate([project], manual);
	}

	public deleteAttribution(evaluator: Evaluator, project: Project): void {
		evaluator.removeProjectsToEvaluate([project]);
	}

	public authenticate(connexionToken: string, tokenExpiry: number): void {
		// TODO: Improve the way the services orders the repository to persist the data.
	}
}
