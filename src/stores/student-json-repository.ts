import { writeFileSync } from "fs";

import { IStudentRepository } from "../features/i-repository";
import { Student } from "../entities/student";
import { StudentRepository } from "./student-repository";
import { tryReadOrReset } from "./restore-data";
import { BaseJSONRepository } from "./base-json-repository";

export class StudentJsonRepository extends BaseJSONRepository<Student> implements IStudentRepository {
	public repository: StudentRepository = new StudentRepository();
	private studentById: Map<string, Student> = new Map();

	private constructor(savePath: string) {
		super(savePath);
	}

	public static restoreFromJson(path: string): StudentJsonRepository {
		const repository = new StudentJsonRepository(path);
		const students: Student[] = tryReadOrReset(path);

		for (let studentData of students) {
			const student = new Student(
				studentData.id,
				studentData.firstname,
				studentData.lastname,
				studentData.email,
				studentData.semester,
				Student.getFiliereFromString(studentData.filiere),
			);

			repository.studentById.set(studentData.id, student);
			repository.repository.create(student);
		}

		return repository;
	}

	public serialize(): void {
		const content = JSON.stringify(this.repository.entities);
		writeFileSync(this.savePath, content, "utf-8");
	}

	//#region IStudentRepository
	getByFirstLastName(firstname: string, lastname: string): Student | null {
		return this.repository.getByFirstLastName(firstname, lastname);
	}
	//#endregion
}
