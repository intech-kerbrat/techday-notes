import { readFileSync, writeFileSync } from "fs";

export function tryReadOrReset<T>(path: string): T[] {
	let criterias: T[];
	let content: string;

	try {
		content = readFileSync(path, "utf-8");
	} catch (e) {
		content = "[]";
		writeFileSync(path, content, "utf-8");
	}

	try {
		criterias = JSON.parse(content);
	} catch (e) {
		criterias = [];
	}

	return criterias;
}
