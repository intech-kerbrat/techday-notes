import { Evaluator } from "../entities/evaluator";
import { Grade } from "../entities/grade";
import { IGradeRepository } from "../features/i-repository";
import { BaseRepository } from "./base-repository";

export class GradeRepository extends BaseRepository<Grade> implements IGradeRepository {
	// IRepository<T>
	public update(id: string, values: Grade): void {
		const grade = this.getById(id);

		if (grade !== null) {
			grade.value = values.value;
		}
	}

	// IGradeRepository
}
