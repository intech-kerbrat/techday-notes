import { Entity } from "./entity";
import { Filiere } from "./filiere";
import { Student } from "./student";

export class Project extends Entity {
	public readonly students: Student[] = [];

	constructor(
		id: string,
		public name: string,
		public semester: number,
		public filiere: Filiere,
		public video_url?: string,
		public video_type?: VideoType,
		public active: boolean = true,
	) {
		super(id);
	}

	public addStudent(student: Student): void {
		if (this.students.includes(student) === false) {
			this.students.push(student);
		}
	}

	public static parseVideoType(str: string): VideoType {
		if (str === "hosted") {
			return VideoType.hosted;
		} else {
			return VideoType.youtube;
		}
	}
}

export enum VideoType {
	hosted = "hosted",
	youtube = "youtube",
}
