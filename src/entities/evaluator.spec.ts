import * as chai from "chai";
import "mocha";
const assert = chai.assert;

import { Criteria } from "./criteria";
import { Evaluator } from "./evaluator";
import { EvaluatorType } from "./evaluator-type";
import { Filiere } from "./filiere";
import { Grade } from "./grade";
import { Project } from "./project";

const criterias: Criteria[] = [
	new Criteria("a", "c1", "text"),
	new Criteria("a", "c2", "text"),
];

const projects: Project[] = [
	new Project("a", "project1", 1, Filiere.none),
	new Project("a", "project2", 1, Filiere.none),
];

describe("Evaluator", () => {
	let sut: Evaluator;

	beforeEach(() => {
		sut = new Evaluator("a", "abc@site.com", "Abc SITE", EvaluatorType.parent);
	});

	describe("authenticate", () => {
		it("should set the connexion token", () => {
			sut.authenticate("xxx");
			assert.strictEqual(sut.connexionToken, "xxx");
		});

		it("should set the maxAge based on the static constant on the class", () => {
			sut.authenticate("xxx");

			assert.isTrue(sut.tokenExpiry !== undefined);
			assert.isTrue(sut.tokenExpiry !== undefined && sut.tokenExpiry > Date.now());
			assert.strictEqual(sut.tokenMaxAge, Evaluator.MAX_AGE);
		});
	});

	describe("hasToEvaluate", () => {
		it("sould tell the evaluator has to watch the project", () => {
			sut.registerProjectsToEvaluate([projects[0]], false);
			assert.isTrue(sut.hasToEvaluate(projects[0]));
		});

		it("sould tell the evaluator must not watch the project", () => {
			sut.registerProjectsToEvaluate([projects[0]], false);
			assert.isFalse(sut.hasToEvaluate(projects[1]));
		});
	});

	describe("registerProjectsToEvaluate", () => {
		it("should register the projects to evaluate", () => {
			sut.registerProjectsToEvaluate(projects, false);

			assert.strictEqual(sut.to_evaluate.length, 2);
			assert.isTrue(sut.hasToEvaluate(projects[0]));
			assert.isTrue(sut.hasToEvaluate(projects[1]));
		});
	});

	describe("removeProjectsToEvaluate", () => {
		it("should register the projects to evaluate", () => {
			sut.registerProjectsToEvaluate(projects, false);

			sut.removeProjectsToEvaluate([projects[1]]);

			assert.strictEqual(sut.to_evaluate.length, 1);
			assert.isFalse(sut.hasToEvaluate(projects[1]));
		});

		it("should remove the grades associated with the project", () => {
			sut.registerProjectsToEvaluate([projects[0]], false);
			sut.grades.push(new Grade("g1", projects[0], criterias[0], 1));
			sut.grades.push(new Grade("g2", projects[0], criterias[1], 1));

			sut.removeProjectsToEvaluate([projects[0]]);

			assert.strictEqual(sut.grades.length, 0);
		});
	});

	describe("hasWatchedAllVideos", () => {
		it("should compute that it has not watched all videos yet", () => {
			sut.registerProjectsToEvaluate(projects, false,);
			// Missing tree grades
			sut.grades.push(new Grade("a", projects[0], criterias[0], 1));

			const result = sut.hasWatchedAllVideos(criterias);

			assert.isFalse(result);
		});

		it("should compute that it has watched all videos", () => {
			sut.registerProjectsToEvaluate(projects, false);
			sut.grades.push(new Grade("a", projects[0], criterias[0], 1));
			sut.grades.push(new Grade("a", projects[0], criterias[1], 1));
			sut.grades.push(new Grade("a", projects[1], criterias[0], 1));
			sut.grades.push(new Grade("a", projects[1], criterias[1], 1));

			const result = sut.hasWatchedAllVideos(criterias);

			assert.isTrue(result);
		});
	});

	describe("isConnected", () => {
		it("should confirm the user is connected", () => {
			sut.authenticate("xxx");
			const result: boolean = sut.isConnected();
			assert.isTrue(result);
		});

		it("should tell the user is deconnected", () => {
			// Missing call to authenticate method.
			const result: boolean = sut.isConnected();
			assert.isFalse(result);
		});
	});
});
