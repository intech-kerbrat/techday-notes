export enum EvaluatorType {
	parent = "parent",
	student = "student",
	employee = "employee",
	alumni = "alumni",
	partner = "partner",
	other = "other",
}
