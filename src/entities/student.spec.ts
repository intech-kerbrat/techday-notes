
// ===== Imports ===== //

import { assert } from "chai";
import "mocha";

import { Student } from "./student";

// ===== Tests ===== //

describe("Student", () => {
	describe("parseFirstAndLastNames", () => {
		it("should parse for firstname and lastname containing spaces", () => {
			const [firstname, lastname] = Student.parseFirstAndLastNames("First First LAST LAST");

			assert.strictEqual(firstname, "First First");
			assert.strictEqual(lastname, "LAST LAST");
		});

		it("should parse for lastname containing hyphen", () => {
			const [firstname, lastname] = Student.parseFirstAndLastNames("First LAST-LAST");

			assert.strictEqual(firstname, "First");
			assert.strictEqual(lastname, "LAST-LAST");
		});

		it("should parse for single word capitalized firstname and lastname separated by a single space", () => {
			const [firstname, lastname] = Student.parseFirstAndLastNames("First Last");

			assert.strictEqual(firstname, "First");
			assert.strictEqual(lastname, "Last");
		});

		it("should not return undefined for the lastname when there is a single word as an input", () => {
			const [firstname, lastname] = Student.parseFirstAndLastNames("First");

			assert.strictEqual(firstname, "First");
			assert.notStrictEqual(lastname, undefined);
		})
	});
});
