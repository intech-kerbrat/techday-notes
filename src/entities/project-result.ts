import { Project } from "./project";

export class ProjectResult {
	constructor(
		public readonly project: Project,
		public readonly value: number,
	) { }
}
