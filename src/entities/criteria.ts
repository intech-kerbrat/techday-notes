import { Entity } from "./entity";

export class Criteria extends Entity {
	constructor(
		id: string,
		public name: string,
		public description: string,
		public weight: number = 1,
	) {
		super(id);
	}
}
