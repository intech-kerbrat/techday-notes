import { Entity } from "./entity";
import { Filiere } from "./filiere";

export class Student extends Entity {
	constructor(
		id: string,
		public firstname: string,
		public lastname: string,
		public email: string,
		public semester: number,
		public filiere: Filiere,
		public linkedin?: string,
		public phone_number?: string,
	) {
		super(id);
	}

	static getFiliereFromString(filiere: string): Filiere {
		const filiere_upper = filiere.toUpperCase();
		return filiere_upper === "IL"
			? Filiere.il
			: filiere_upper === "SR" || filiere_upper === "ASI"
				? Filiere.asi
				: Filiere.none;
	}

	/**
	 * Parse first and last name, including when they themselves contain spaces, or try with simpler one-word first and last names.
	 * @param firstlastname A string containing first name and last name separated by spaces.
	 */
	static parseFirstAndLastNames(firstlastname: string): string[] {
		const words = firstlastname.trim().split(" ");
		const nameStartIndex = words.findIndex(word => Student.wordCaseRatio(word) === 1);
		let firstname = "";
		let lastname = "";

		if (nameStartIndex !== -1) {
			firstname = words.splice(0, nameStartIndex).join(" ");
			lastname = words.join(" ");
		} else {
			if (words.length >= 1) {
				firstname = words[0];
			}

			if (words.length >= 2) {
				lastname = words[1];
			}
		}

		return [firstname, lastname];
	}

	/**
	 * Computes the ratio of upper versus lower case.
	 * @returns 1 is all upper case, 0 is all lower case.
	 */
	private static wordCaseRatio(value: string): number {
		function isLower(value: string): boolean { return value >= "a" && value <= "z"; }
		function isUpper(value: string): boolean { return value >= "A" && value <= "Z"; }
		let upperCount = 0;
		let total = 0;

		for (let char of value) {
			upperCount += isUpper(char) ? 1 : 0;
			total += (isLower(char) || isUpper(char) ? 1 : 0);
		}

		return upperCount / total;
	}
}
