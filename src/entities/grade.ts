import { Criteria } from "./criteria";
import { Entity } from "./entity";
import { Project } from "./project";

export class Grade extends Entity {
	constructor(
		id: string,
		public project: Project,
		public criteria: Criteria,
		public value: number
	) {
		super(id);
	}
}
